\documentclass[12pt,a4paper]{article}

\usepackage{amsmath}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{subcaption}

\begin{document}
\begin{titlepage}
	\centering
	{\scshape\LARGE University of Leeds \par}
	{\scshape\Large Facility of Engineering\par}
	{\scshape\Large School of Computing\par}
	\vspace{2cm}
	{\LARGE\bfseries COMP 3811\par}
	{\LARGE\bfseries Computer Graphics\par}
	{\LARGE\bfseries Coursework 1\par}
	\vspace{3cm}
	{\Large\itshape Sam Barnes\par}
	{\Large\itshape sc18sjb\par}
	\vfill
	
% Bottom of the page
	{\large \hrule
	\begin{flushleft}
		\textbf{Submission date:} 5th November 2021, 17:00\\
	\end{flushleft}
	\par}
\end{titlepage}

\newpage



\section{Assignment 1: Drawing a line}
We are asked to draw a line between two points $Q$ and $P$ using the parametrised version of a line, that is $(1-t)P + tQ$ where t is $0 < t < 1$. $t$ is out 'step', that is if we increase the value of $t$ between $0$ and $1$ we 'walk' between the two points $P$ and $Q$, drawing our line. We can define a method \textit{DrawLine( px, py, qx, qy )} to facilitate this: 

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q1_code.png}
  \end{subfigure}
  
  \textbf{\textit{DrawLine( px, py, qx, qy )} method }
\end{figure}

We start by defining our step size $t_{step}$ to be an arbitrary number that is small enough to hit every pixel in the line, but not too large to be wasteful. I define my step size $t_{step}$ to be the distance $D$ between the two points $P$ and $Q$; $t_{step} = 1D$. We can easily define this as $D = \sqrt{(Q_0 - P_0)^2 + (Q_1 - P_1)^2}$. This ensures the number of pixels hit is exactly the number of pixels needed to draw the line and no less. We then calculate the parametrised version of a line for each point $t$ between $0 < t < 1$ to get the $x$ and $y$ coordinates for each pixel in the line.

\newpage

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q1_output.png}
  \end{subfigure}
  
  \textbf{\textit{DrawLine( px, py, qx, qy )} output with $t_{step} = 1D$ }
\end{figure}

I call the method \textit{DrawLine( px, py, qx, qy )} 4 times to draw 4 white lines seen above. We can see these lines light up the pixels that contains the mathematical ideal of the line and no more such as to be the minimum step size to hit every pixel in the line.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q1_output_2.png}
  \end{subfigure}
  
  \textbf{\textit{DrawLine( px, py, qx, qy )} output with $t_{step} = 5D$ }
\end{figure}

In my code I am setting the $t_{step} = 5D$ since from the outputs produced, these lines had a better portrayal with less artefacts. This means the step size calculated to be the least wasteful $t_{step}$ $* 5$ for a better output.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q1_output_expected.png}
  \end{subfigure}
  
  \textbf{\textit{DrawLine( px, py, qx, qy )} expected output }
\end{figure}

I check the outputs above are expected by drawing each of the 4 lines using the standard QT QPainter drawline method (seen in red). When I compare my lines to the lines drawn by QT I can see they are expected as they follow the ideal of the QT line with the first pixels top left being the $P_x$,  $P_y$ coordinate and the last pixels top left being the $Q_x$, $Q_y$ coordinate which is expected and is also shown with the QT line. 

\section{Assignment 2: Interpolating the colour along a long}
We can adapt our \textit{DrawLine( px, py, qx, qy )} method to accept two RGB values, one for the start point and one for the end point, and interpolate the colours between the two points along the line. I have defined a new method \textit{DrawColorLine( px, py, qx, qy, color1, color2 )} to facilitate this:

\newpage

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q2_code.png}
  \end{subfigure}
  
  \textbf{\textit{DrawColorLine( px, py, qx, qy, color1, color2 )} method }
\end{figure}

We can see the only differences between this method and our previous method are the addition of 3 new lines that interpolate the colors. We do this using the same parametrised version of the line but instead of 'walking' between two points we 'walk' between two colors along the same $t_{step}$. Say color1 = $P$ and color2 = $Q$ then the formula is the same $(1-t)P + tQ$ where t is $0 < t < 1$. We calculate this for each of the 3 RGB values c1, c2, c3 which are then used in the \textit{SetPixel(x, y, color)} call as the associated colour RGB values.

\newpage

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q2_output.png}
  \end{subfigure}
  
  \textbf{\textit{DrawColorLine( px, py, qx, qy, color1, color2 )} output }
\end{figure}

I call my \textit{DrawColorLine( px, py, qx, qy, color1, color2 )} method 4 times with the same lines drawn for assignment 1, interpolating between 2 arbitrary colours. My results are expected because the lines are drawn the same as assignment 1 (since I am using the same code for this) which I have already defined to be expected and the colours interpolated correctly.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q2_call.png}
  \end{subfigure}
  
  \textbf{\textit{Call to DrawColorLine( px, py, qx, qy, color1, color2 )} method }
\end{figure}

Here are my calls to the \textit{DrawColorLine( px, py, qx, qy, color1, color2 )} method with the associated RGB values for colours 1 and 2. We can see the output is as expected since, as an example, if we take line 1 we are interpolating from colors red to green and on the output we can see that is what happens, with a nice greeny-orange interpolation in-between the two colours. We get the same for the other lines so can say the output is as expected.

\newpage

\section{Assignment 3: Interpolating pixels that are part of a triangle}
We are asked to draw a triangle from 3 given points $P$, $Q$, $R$ and interpolate the pixels that are part of the triangle using the barycentric coordinates scheme. That is, we wish to solve the weights for $P$, $Q$, $R$ as $\alpha$, $\beta$, $\gamma$ for any given point $X$. We can solve this with the follow equations:

\vspace{5mm}
\begin{center}
$\alpha = \frac{(Q_y - R_y)(X_x - R_x) + (R_x - Q_x)(X_y - R_y)}{(Q_y - R_Y)(P_x - R_x) + (R_X - Q_x)(P_y - R_y)}$

\vspace{2mm}
$\beta = \frac{(R_y - P_y)(X_x - R_x) + (P_x - R_x)(X_y - R_y)}{(Q_y - R_Y)(P_x - R_x) + (R_X - Q_x)(P_y - R_y)}$

\vspace{2mm}
$\gamma = 1 - \alpha - \beta$
\end{center}

To check if $X$ is inside the triangle we simply need to check if any of the weights $\alpha$, $\beta$, $\gamma$ are negative and if so, we know $X$ is outside the triangle (and thus if all the weights are $>= 0$ we know $X$ is inside the triangle). I have defined a method \textit{DrawTriangle( px, py, qx, qy, rx, ry, color1, color2, color3 )} to facilitate this:

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q3_code.png}
  \end{subfigure}
  
  \textbf{\textit{DrawTriangle( px, py, qx, qy, rx, ry, color1, color2, color3 )} method }
\end{figure}

We start by looping over every pixel in the 70 x 70 grid. We then calculate the barycentric coordinates of each of the pixels using the formulas for $\alpha$, $\beta$, $\gamma$. Is it then simply a case of checking if all $\alpha$, $\beta$, $\gamma$ $>= 0$ then we know the pixel is inside the triangle and we can draw it. We can interpolate the colours between the 3 points $P$,$Q$, $R$ again using the barycentric coordinates scheme simply by multiplying the weights $\alpha$, $\beta$, $\gamma$ against the RGB values of each colour to form a new colour. We can see this defined in the \textit{MultiplyColor( bary, color )} method. It is then simply a case of summing all the RGB values from the 3 colors c1, c2, c3 produced from the 3 weights $\alpha$, $\beta$, $\gamma$ to form the colour of the pixel.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q3_output.png}
  \end{subfigure}
  
  \textbf{\textit{DrawTriangle( px, py, qx, qy, rx, ry, color1, color2, color3 )} output   }
\end{figure}

I call my \textit{DrawTriangle( px, py, qx, qy, rx, ry, color1, color2, color3 )} method with the 3 colours red, green and blue (in that order) to produce the triangle above. I can see my triangle has a nice interpolation between the 3 colours as expected with all the colours interpolating towards the center of the triangle.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q3_call.png}
  \end{subfigure}
  
  \textbf{\textit{Call to DrawTriangle( px, py, qx, qy, rx, ry, color1, color2, color3 )} output   }
\end{figure}

Here we can see my call to the \textit{DrawTriangle( px, py, qx, qy, rx, ry, color1, color2, color3 )} method with the coordinates for $P$, $Q$, $R$ and their associated colour values. For example we can see the point $R$ is at (5, 54) with a colour of blue which is reflected in the output so we can say the results are expected as this is the same for the other points $P$ and $Q$ too.


\newpage
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q3_output_expected.png}
  \end{subfigure}
  
  \textbf{\textit{DrawTriangle( px, py, qx, qy, rx, ry, color1, color2, color3 )} expected output   }
\end{figure}

To check the barycentric coordinates have drawn the triangle correctly I am again using the standard QT QPainter drawline method to draw the 3 edges of the triangle (seen in white). We can see the edges of the triangle nicely follow the ideal of these lines as expected and hit all the pixels along them and inside the triangle.

\section{Assignment 4: Half plane test for determining if a point is inside or outside a triangle}

We are asked to implement a half plane test for determining whether a given point $X$ is inside or outside a triangle defined by the points $P$, $Q$ and $R$. A half plane test is carried out on one edge of the triangle 3 times for each edge of the triangle. That is, we work out the normal vector $\vec{n}$ for a given edge defined by two points $P^\prime$ and $Q^\prime$ such that $\vec{n} = \times P^\prime Q^\prime$ and then calculate the equation of the line through $P^\prime$ which has a normal $\vec{n}$ with $(X - P^\prime)\cdot \vec{n} = 0$ for an arbitrary point $X$. 

We can take this equation of the line to check which side of the line $X$ is on which is called our half plane test. let $f(X)\equiv (X - P^\prime)\cdot \vec{n} = 0$. if $f(X) > 0$ then we are 'above' the line and on the same side as the normal, if $f(X) < 0$ then we are 'below' the line on the opposite side as the normal. Thus, to check if a point $X$ is inside the triangle we simply conduct 3 half plane tests for each edge of the triangle and if each test holds true for the function $f(X) > 0$ we know $X$ is inside the triangle. It is worth noting in my method I am conducting the test $f(X) >= 0$ since if the point is on the edge of the triangle (where $f(X) = 0$) then I am still counting this as being inside the triangle). I have defined a method \textit{IsInside( px, py, qx, qy, rx, ry, x, y )} to facilitate this:

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q4_code.png}
  \end{subfigure}
  
  \textbf{\textit{IsInside( px, py, qx, qy, rx, ry, x, y )} method and associated call }
\end{figure}

I am calling my \textit{IsInside( px, py, qx, qy, rx, ry, x, y )} method for every pixel in the 70 x 70 grid, simply returning a bool for if the point is inside or outside the triangle. My \textit{IsInside(px, py, qx, qy, rx, ry, x, y)} method conducts 3 half plane test a, b, c from the method \textit{CalulateNormal( px, py, qx, qy, x, y )} which, if all true, shows the point $X$ is inside the triangle. My \textit{CalculateNormal( px, py, qx, qy, x, y )} method simply conducts the function $f(X)\equiv (X - P^\prime)\cdot \vec{n} >= 0$ using the dot product formula 

\begin{center}
$a \cdot b = a_x \times b_x + a_y \times b_y $
\end{center}

\noindent
for $a = (X-P^\prime)$ and $b = \vec{n}$.

I am outputting the results for every pixel in the 70 x 70 grid into a file which, along with the barycentric coordinates of each pixel, can be compared to see whether the results are expected or not.


\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q4_comparison.png}
  \end{subfigure}
  
  \textbf{\textit{evaluate\textunderscore output.py} comparison script }
\end{figure}

I wrote a comparison script in python, \textit{evaluate\textunderscore output.py}, to check through all the results output to the file from the \textit{IsInside( px, py, qx, qy, rx, ry, x, y )} method. The script splits up the data into its individual components: $\alpha$, $\beta$, $\gamma$ and the results from the half plane test (bool). It then checks if all $\alpha$, $\beta$, $\gamma$ $>= 0$ and the half plane test results are true then the results are expected since the barycentric coordinates scheme and half plane test results match; both define the point as being inside the triangle. However, if the barycentric coordinates say the point is inside the triangle but the half plane test does not (or vice-versa), then the results are unexpected; they do not match. I simply output any unexpected results at the end of the script or, if none are found, state that all results are expected.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q4_comparison_output.png}
  \end{subfigure}
  
  \textbf{\textit{evaluate\textunderscore output.py} comparison script output}
\end{figure}

I called my \textit{IsInside( px, py, qx, qy, rx, ry x, y )} method on the same triangle defined in assignment 3 to output the results from the half plane test and barycentric coordinates into the file 'output.txt' (available in the code repository). I know the results from the barycentric coordinates of this triangle are expected, since this has already been discussed in assignment 3, so it is simply a case of comparing these with the results of the half plane test. When I ran my \textit{evaluate\textunderscore output.py} comparison script I see that it is a perfect match; all the barycentric coordinates and half plane test results match, so in this way the results are expected.

\section{Assignment 5: Creating a ppm image of an interpolated triangle}

We are asked to create a PMM image from an interpolated triangle. I have extended my answer to assignment 3 to use the barycentric coordinates of the interpolates triangle as the results for the PPM file. I have defined a method \textit{TriangleToPPM( px, py, qx, qy, rx, ry, color1, color2, color3 )} to facilitate this:

\newpage
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q5_code.png}
  \end{subfigure}
  
  \textbf{\textit{TriangleToPPM( px, py, qx, qy, rx, ry, color1, color2, color3 )} method}
\end{figure}

We can see in my \textit{TriangleToPPM( px, py, qx, qy, rx, ry, color1, color2, color3 )} method I am defining the header of the PPM image, that is I am setting it to a P3 format image with a size of 70 x 70 (the horizontal and vertical coordinates) and a max RGB value of 255. I then simply calculate the barycentric coordinates as defined in assignment 3 and output the colour values into the PPM file, or if the coordinates are outside the triangle, output a black pixel.

\newpage
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1\linewidth}
    \includegraphics[width=\linewidth]{q5_output.png}
  \end{subfigure}
  
  \textbf{\textit{TriangleToPPM( px, py, qx, qy, rx, ry, color1, color2, color3 )} method output}
\end{figure}

Here we can see the output of my \textit{TriangleToPPM( px, py, qx, qy, rx, ry, color1, color2, color3 )} method in the graphical image manipulation program GIMP. This is accessible in the 'triangle.ppm' file in the repository for you to look at yourself. Running the ./pixelate application will generate a new image for whatever triangle is defined.


\end{document}