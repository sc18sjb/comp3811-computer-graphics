#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <QDesktopWidget>
#include <iostream>
#include <string>
#include <fstream>
#include <math.h>
#include "pixelwidget.hpp"

using namespace std;

void PixelWidget::DefinePixelValues(){ //add pixels here; write methods like this for the assignments
  SetPixel(30,30,RGBVal(255,0,0));
  SetPixel(30,45,RGBVal(255,255,255));
}


void PixelWidget::DrawLine(float px, float py, float qx, float qy) {
  float nPoints = (sqrt(pow(qx - px, 2) + pow(qy - py, 2)) * 5);
  float t = 0;

  for (int i = 0; i < nPoints; i++) {
    float xs = t * px + (1 - t) * qx;
    float ys = t * py + (1 - t) * qy;
    
    SetPixel(xs, ys, RGBVal(255, 255, 255));
    t += (1.0 / nPoints);
  }

  // the first and last pixel will always be lit up
  SetPixel(px, py, RGBVal(255, 255, 255));   
  SetPixel(qx, qy, RGBVal(255, 255, 255)); 
}


void PixelWidget::DrawColorLine(float px, float py, float qx, float qy, RGBVal color1, RGBVal color2) {
  float nPoints = (sqrt(pow(qx - px, 2) + pow(qy - py, 2)) * 5);
  float t = 0;

  for (int i = 0; i < nPoints; i++) {
    // interpolate the colors
    float c1 = t * color1._red + (1 - t) * color2._red;
    float c2 = t * color1._green + (1 - t) * color2._green;
    float c3 = t * color1._blue + (1 - t) * color2._blue;

    // set the line values between the points
    float xs = t * px + (1 - t) * qx;
    float ys = t * py + (1 - t) * qy;

    SetPixel(xs, ys, RGBVal(c1, c2, c3));
    t += (1.0 / nPoints);
  }

  // the first and last pixel will always be lit up
  SetPixel(px, py, color1);
  SetPixel(qx, qy, color2);
}

RGBVal PixelWidget::MultiplyColor(float bary, RGBVal color) {
  float c1 = bary * color._red;
  float c2 = bary * color._green;
  float c3 = bary * color._blue;

  return RGBVal(c1, c2, c3);
}


void PixelWidget::DrawTriangle(float px, float py, float qx, float qy, float rx, float ry, RGBVal color1, RGBVal color2, RGBVal color3) {  
  for (unsigned int x = 0; x < _n_horizontal; x++) {
    for (unsigned int y = 0; y < _n_vertical; y++) {
      // Calculcate the barycentric coordinates      
      float alpha = ((qy - ry) * (x - rx) + (rx - qx) * (y - ry)) / ((qy - ry) * (px - rx) + (rx - qx) * (py - ry));
      float beta = ((ry - py) * (x - rx) + (px - rx) * (y - ry)) / ((qy - ry) * (px - rx) + (rx - qx) * (py - ry));
      float gamma = 1.0 - alpha - beta;

      // prevent rounding errors
      if (alpha + beta == 1)
        gamma = 0.0;

      if (alpha >= 0.0 && beta >= 0.0 && gamma >= 0.0) {
        // Multiple each color by alpha, beta, gamma
        RGBVal c1 = MultiplyColor(alpha, color1);
        RGBVal c2 = MultiplyColor(beta, color2);
        RGBVal c3 = MultiplyColor(gamma, color3);

        // Sum all the values together
        RGBVal color = RGBVal(c1._red + c2._red + c3._red, c1._green + c2._green + c3._green, c1._blue + c2._blue + c3._blue);

        SetPixel(x, y, color);
      }
    }
  }
}


bool PixelWidget::CalculateNormal(float px, float py, float qx, float qy, float x, float y) {
  return ((x - px) * -(qy - py) + (y - py) * (qx - px)) >= 0 ? true : false;
}


bool PixelWidget::IsInside(float px, float py, float qx, float qy, float rx, float ry, float x, float y) {
  bool a = CalculateNormal(px, py, qx, qy, x, y);
  bool b = CalculateNormal(qx, qy, rx, ry, x, y);
  bool c = CalculateNormal(rx, ry, px, py, x, y);
  
  return (a && b && c) ? true : false;
}


void PixelWidget::HalfPlaneTest(float px, float py, float qx, float qy, float rx, float ry) {
  ofstream output("output.txt");

  for (unsigned int x = 0; x < _n_horizontal; x++) {
    for (unsigned int y = 0; y < _n_vertical; y++) {
      // Calculate the barycentric coordinates
      float alpha = ((qy - ry) * (x - rx) + (rx - qx) * (y - ry)) / ((qy - ry) * (px - rx) + (rx - qx) * (py - ry));
      float beta = ((ry - py) * (x - rx) + (px - rx) * (y - ry)) / ((qy - ry) * (px - rx) + (rx - qx) * (py - ry));
      float gamma = 1.0 - alpha - beta;

      // prevent rounding errors
      if (alpha + beta == 1)
        gamma = 0.0;

      // Fetch and write the results to the output
      bool result = IsInside(px, py, qx, qy, rx, ry, x, y);
      output << "x: " << x << ", y: " << y << std::flush; 
      output << ", alpha: " << alpha << ", beta: " << beta << ", gamma: " << gamma << std::flush;
      output << ", Half plane test result: " << result << " \n";
    }
  }

  output.close();
}

void PixelWidget::TriangleToPPM(float px, float py, float qx, float qy, float rx, float ry, RGBVal color1, RGBVal color2, RGBVal color3) {
  ofstream output("triangle.ppm"); 

  // Write the header of the ppm file
  output << "P3\n";
  output << _n_horizontal << " " << _n_vertical << "\n";
  output << "255\n";
  
  for (unsigned int y = 0; y < _n_vertical; y++) {
    for (unsigned int x = 0; x < _n_horizontal; x++) {
      // Calculcate the barycentric coordinates      
      float alpha = ((qy - ry) * (x - rx) + (rx - qx) * (y - ry)) / ((qy - ry) * (px - rx) + (rx - qx) * (py - ry));
      float beta = ((ry - py) * (x - rx) + (px - rx) * (y - ry)) / ((qy - ry) * (px - rx) + (rx - qx) * (py - ry));
      float gamma = 1.0 - alpha - beta;
      
      // prevent rounding errors
      if (alpha + beta == 1)
        gamma = 0.0;

      if (alpha >= 0.0 && beta >= 0.0 && gamma >= 0.0) {
        // Multiple each color by alpha, beta, gamma
        RGBVal c1 = MultiplyColor(alpha, color1);
        RGBVal c2 = MultiplyColor(beta, color2);
        RGBVal c3 = MultiplyColor(gamma, color3);

        // Sum all the values together
        RGBVal color = RGBVal(c1._red + c2._red + c3._red, c1._green + c2._green + c3._green, c1._blue + c2._blue + c3._blue);

        // Write color value to ppm
        output << color._red << " " << color._green << " " << color._blue << "   ";
      } else {
        // if not inside triangle, write black
        output << "0  0  0   ";
      }
    }
  }

  output.close();
}


// -----------------Most code below can remain untouched -------------------------
// ------but look at where DefinePixelValues is called in paintEvent--------------

PixelWidget::PixelWidget(unsigned int n_vertical, unsigned int n_horizontal):
  _n_vertical   (n_vertical),
  _n_horizontal (n_horizontal),
  _vec_rects(0)
{
  // all pixels are initialized to black
     for (unsigned int i_col = 0; i_col < n_vertical; i_col++)
       _vec_rects.push_back(std::vector<RGBVal>(n_horizontal));
}



void PixelWidget::SetPixel(unsigned int i_column, unsigned int i_row, const RGBVal& rgb)
{

  // if the pixel exists, set it
  if (i_column < _n_vertical && i_row < _n_horizontal)
    _vec_rects[i_column][i_row] = rgb;
}


void PixelWidget::paintEvent( QPaintEvent * )
{

  QPainter p( this );
  // no antialiasing, want thin lines between the cell
  p.setRenderHint( QPainter::Antialiasing, false );

  // set window/viewport so that the size fits the screen, within reason
  p.setWindow(QRect(-1.,-1.,_n_vertical+2,_n_horizontal+2));
  int side = qMin(width(), height());  
  p.setViewport(0, 0, side, side);

  // black thin boundary around the cells
  QPen pen( Qt::black );
  pen.setWidth(0.);

  // here the pixel values defined by the user are set in the pixel array
  DrawColorLine(2, 2, 35, 10, RGBVal(255, 0, 0), RGBVal(0, 255, 0));
  DrawColorLine(15, 30, 30, 60, RGBVal(0, 255, 0), RGBVal(0, 0, 255));
  DrawColorLine(69, 69, 61, 51, RGBVal(165, 241, 45), RGBVal(23, 157, 231));
  DrawColorLine(34, 40, 53, 27, RGBVal(43, 56, 211), RGBVal(188, 27, 233));
  DrawTriangle(15.0, 10.0, 35.0, 30.0, 5.0, 24.0, RGBVal(255, 0, 0), RGBVal(0, 255, 0), RGBVal(0, 0, 255));
  DrawTriangle(41.0, 52.0, 53.0, 60.0, 32.0, 67.0, RGBVal(134, 34, 243), RGBVal(45, 76, 145), RGBVal(217, 155, 34));
  TriangleToPPM(15.0, 10.0, 55.0, 60.0, 5.0, 54.0, RGBVal(255, 0, 0), RGBVal(0, 255, 0), RGBVal(0, 0, 255));
  HalfPlaneTest(15.0, 10.0, 55.0, 60.0, 5.0, 54.0);

  for (unsigned int i_column = 0 ; i_column < _n_vertical; i_column++)
    for(unsigned int i_row = 0; i_row < _n_horizontal; i_row++){
      QRect rect(i_column,i_row,1,1);
      QColor c = QColor(_vec_rects[i_column][i_row]._red, _vec_rects[i_column][i_row]._green, _vec_rects[i_column][i_row]._blue);
    
      // fill with color for the pixel
      p.fillRect(rect, QBrush(c));
      p.setPen(pen);
      p.drawRect(rect);
    }
}
