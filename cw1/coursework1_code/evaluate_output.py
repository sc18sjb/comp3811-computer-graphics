def parseResults() -> tuple:
    expected: list = []
    notExpected: list = []

    with open("output.txt") as f:
        for line in f:
            # Split the data into individual components
            data: list = line.strip("\n").strip().split(",")

            # Read each component into data type
            alpha: float = float(data[2].split(":")[1].strip())
            beta: float = float(data[3].split(":")[1].strip())
            gamma: float = float(data[4].split(":")[1].strip())
            halfPlaneTest: bool = True if data[5].split(":")[1].strip() == "1" else False

            # If bary is true then we check if half plane test is also true
            if alpha >= 0.0 and beta >= 0.0 and gamma >= 0.0:
                if halfPlaneTest:
                    expected.append(line)
                else:
                    notExpected.append(line)
            # if bary is not true but half plane test is true this is unexpected
            elif halfPlaneTest:
                notExpected.append(line)
            # if bary is not true and half plane test is not true this is expected
            else:
                expected.append(line)

    return expected, notExpected
    
def main() -> None:
    expected: list = []
    notExpected: list = []
    expected, notExpected = parseResults()

    # Show the unexpected!
    if len(notExpected) > 0:
        for line in notExpected:
            print(line)
    else:
        print("All barycentric and half plane test results are equal!")

if __name__ == "__main__":
    main()
    