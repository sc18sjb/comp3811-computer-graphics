#ifndef _PIXEL_WIDGET_
#define _PIXEL_WIDGET_

#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <QDesktopWidget>
#include <iostream>
#include <vector>
#include "RGBVal.hpp"


class PixelWidget : public QWidget {
public:

  
  // set the number of pixels that the widget is meant to display
  PixelWidget
  (
   unsigned int n_horizontal, // the first integer determines the number of horizontal pixels 
   unsigned int n_vertical    // the second integer determines the number of vertical pixels
   );

  // sets a pixel at the specified RGBVal value; ignores non-existing pixels without warning
  void SetPixel
  ( 
   unsigned int  i_x, // horizontal pixel coordinate
   unsigned int  i_y, // vertical pixel coordinate
   const RGBVal& c    // RBGVal object for RGB values
    );

  // Use the body of this function to experiment with rendering algorithms
  void DefinePixelValues();
  void DrawLine(
    float p0,         // x value of starting point
    float p1,         // y value of starting point
    float q0,         // x value of end point
    float q1          // y value of end point
  );

  void DrawColorLine(
    float p0,         // x value of starting point
    float p1,         // y value of starting point
    float q0,         // x value of end point
    float q1,         // y value of end point
    RGBVal color1,    // starting color of the line
    RGBVal color2     // ending color to interpolate to
  );

  void DrawTriangle(
    float px,
    float py,
    float qx,
    float qy,
    float rx,
    float ry,
    RGBVal color1,
    RGBVal color2,
    RGBVal color3
  );

  void TriangleToPPM(
    float px,
    float py,
    float qx,
    float qy,
    float rx,
    float ry,
    RGBVal color1,
    RGBVal color2,
    RGBVal color3
  );

  bool IsInside(
    float px,
    float py, 
    float qx, 
    float qy, 
    float rx, 
    float ry, 
    float x, 
    float y
  );

  void HalfPlaneTest(
    float px,
    float py, 
    float qx, 
    float qy, 
    float rx, 
    float ry
  );

protected:

  virtual void paintEvent(QPaintEvent*);

  RGBVal MultiplyColor(
    float bary,
    RGBVal color
  );

  bool CalculateNormal(
    float px, 
    float py,
    float qx, 
    float xy,
    float x,
    float y
  );


private:

  unsigned int _n_vertical;
  unsigned int _n_horizontal;

  std::vector<std::vector<RGBVal> > _vec_rects;
};

#endif

