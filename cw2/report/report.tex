\documentclass[12pt,a4paper]{article}

\usepackage{amsmath}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{caption}
\usepackage{hyperref}

\begin{document}
\begin{titlepage}
	\centering
	{\scshape\LARGE University of Leeds \par}
	{\scshape\Large Facility of Engineering\par}
	{\scshape\Large School of Computing\par}
	\vspace{2cm}
	{\LARGE\bfseries COMP 3811\par}
	{\LARGE\bfseries Computer Graphics\par}
	{\LARGE\bfseries Coursework 2\par}
	\vspace{3cm}
	{\Large\itshape Sam Barnes\par}
	{\Large\itshape sc18sjb\par}
	\vfill
	
% Bottom of the page
	{\large \hrule
	\begin{flushleft}
		\textbf{Submission date:} 7th January 2022, 17:00\\
	\end{flushleft}
	\par}
\end{titlepage}

\newpage

\tableofcontents

\newpage
\section{Introduction}

We have been tasked with creating a complex scene using the OpenGL C++ graphical framework and QT. The scene in question is up to the author to decide upon but must meet all the requirements of the 4 lower bands described in later sections of this report. This report covers the authors design decisions, implementation choices and methodology to create their scene.

\subsection{Overview}

\begin{center}
	\textbf{Scene Name:} \textit{Sam's Barns by Sam Barnes}
\end{center} 

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.7\linewidth}
    \includegraphics[width=\linewidth]{img/scene_overview.png}
  \end{subfigure}
  
  Figure 1: Final scene overview
\end{figure}

The author has chosen to design a scene based upon a farm, creating various building and objects that would typically be seen in such an environment (such as barns, silos, bushes etc.). The name of the scene derives from the authors name as a fun play on words and is one of the main inspirations behind the design choices of the scene, although these will be described in greater detail at later sections. \textit{Figure 1} gives context to the final scene created. The author will now describe in detail the design and implementation choices that were required to create this context.

\section{Band 1: 40\% \-- 50\%}

The first requirement band is where the bulk of the scene is created, although overlaps to meet the requirements of this band will also meet the requirements of later bands. The requirements for band 1 are as follows:

\begin{itemize}
	\item Create a visual scene that shows reasonable complexity through instancing.
	\item Enable light and material properties to allow specular and diffusive light combinations to be recognised.
\end{itemize}

\subsection{Scene design}
Several iterations of different scene designs were tried before the final scene \textit{Figure 1} displays was decided upon.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.7\linewidth}
    \includegraphics[width=\linewidth]{img/scene_paper_design.png}
  \end{subfigure}
  
  Figure 2: First scene design paper mock-up
\end{figure}

\textit{Figure 2} shows the first of these designs. The author wanted to create a scene with the 'camera' being placed inside a room, pictures of the lecturers Marc de camps and Markus would hang from the wall in picture frames and there would be a spinning globe placed upon a table. This scene would meet all of the above requirements and the idea was to allow the user to control the lighting on or off to display the specular and diffusive properties of the objects. Ultimately this scene was abandoned because the author, due to their lack of experience with OpenGL, could not figure out a way to correctly position the 'camera' inside the room without it clipping with the walls. The author now knows the solution to this problem is to use \textit{gluPerspective} or \textit{gluFrustum} to set the near and far clipping planes, instead of \textit{glOrtho} to set the bounding box.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.7\linewidth}
    \includegraphics[width=\linewidth]{img/scene_pre_design.jpg}
  \end{subfigure}
  
  Figure 3: Final scene inspiration
\end{figure}

\textit{Figure 3} shows the inspiration for what would become the final scene shown in \textit{Figure 1} that was decided upon after the author abandoned their first idea. This is taken from the YouTuber 'ThinnMatrix' city builder development series as the author liked the look of the scene and thought it would be a reasonable challenge to implement, whilst meeting the requirements of band 1 (and later bands). Ultimately, whilst starting creation of this scene (which we can see evidence of in \textit{Figure 1} from the roads, house etc.) it naturally progressed to the final scene we see, after the author realised the pun they could create with the scene name and focussed on creating a scene around a farm. 

\subsection{Requirement 1: complexity through instancing}

Requirement 1 specified creating a scene that shows reasonable complexity through object instancing. This could include (examples taken from the coursework specification) a group of objects constructed from cubes or body shapes constructed from cylinders and spheres. Knowing this we can already see how the scene design \textit{Figure 1} shows meets this requirement.

\subsubsection{Design}
Knowing this requirement and seeing the moc-scene the author wanted to create in \textit{Figure 3}, the author designed a list of basic shapes that would need to be implemented to facilitate creating the scene. This included:

\begin{itemize}
	\item Cube for the building.
	\item Rectangle to create cubes and flat planes.
	\item Triangle for creating wall pieces for the roof to sit on.
	\item Tetrahedron for creating bushed and foliage.
\end{itemize}

Later on this list was expanded to accommodate the additional requirements of the scene, modelling a farm. This included:

\begin{itemize}
	\item Cylinder for barn silo and pig body, tail etc.
	\item Sphere for silo roof and pig head, eyes, ears etc.
\end{itemize}

The author intended to implement all of these shapes their self, with the exception being the sphere that the author intended to implement using a GLUT object, since this made it easier to apply textures to it later.

The following section illustrates examples of the authors design intentions, on how to implement some of the objects described above through instancing. Not every object will be described since there are too many in the scene to go through, but the most 'obvious' forms of instancing will be talked about.

\newpage
\subsubsection{Design intentions}

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.7\linewidth}
    \includegraphics[width=\linewidth]{img/scene_barn.png}
  \end{subfigure}
  
  Figure 4: Barn instance design
\end{figure}

The author designed the barn to be implemented using cubes to represent the building, and a cylinder and sphere to represent the silo attached to the barn. \textit{Figure 4} illustrated this design. The author intends to implement this instance using 6 rectangular planes to create the cube for the building and 2 triangles with 2 more rectangles to create the roof of the building. A convex cylinder and GLUT sphere can be used to create the silo.

\textit{Figure 5} illustrates an example of a bush instance design that can be implemented using tetrahedrons. These tetrahedrons can be made up using 4 triangular faces. Rotating, scaling and translating these tetrahedrons in a random way can create a natural effect akin to a bush.

\textit{Figure 6} illustrates an example of a fence instance design that can be implemented simply using cubes. 2 vertically elongated cubes can be used to represent the poles of the fence and 4 horizontally elongated cubes can be used to represent the planks of the fence.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.7\linewidth}
    \includegraphics[width=\linewidth]{img/scene_bush.png}
  \end{subfigure}
  
  Figure 5: Bush instance design
\end{figure}

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.7\linewidth}
    \includegraphics[width=\linewidth]{img/scene_fence.png}
  \end{subfigure}
  
  Figure 6: Fence instance design
\end{figure}

\newpage

\subsubsection{Implementation}

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{img/shapes_implementation.png}
  \end{subfigure}
  
  Figure 7: Shapes class header file implementation 
\end{figure}

The author created a dedicated Shapes class to implement each of the shapes described in the above design section. \textit{Figure 7} shows the header file for this implementation (without function comments since these would not fit in the screenshot - please look at the code for an extensive description of what each function does). We can see the author successfully implemented each of the shapes they intended to, including the rectangle, triangle, cube, tetrahedron, cylinder and sphere. Several of the same functions were implemented to facilitate texturing (such as for rectangle and triangle).

\newpage

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.9\linewidth}
    \includegraphics[width=\linewidth]{img/triangle_implementaion.png}
  \end{subfigure}
  
  Figure 8: Triangle implementation 
\end{figure}

\textit{Figure 8} illustrates an example of one of the shapes implementations from \textit{Figure 7}, being a triangle. Here we can see the triangle is created by defining the A, B, C vertices for a 1x1 plane, which is recursively subdivided into 4 smaller triangles defined by the \textit{subDivisions}. The vertices of all these smaller triangles are then stored in an array of triangle vertices and returned to the caller, which draws the polygon. The types \textit{Vertex}, \textit{Triangle} and \textit{TriangleVec} definitions can be found at the top of \textit{Figure 7}. We subdivide the triangle to allow for ambient and specular light combinations described in the following section.

* \textbf{Please note the implementation of \textit{Figure 8}, written in C++11, will be different in the final, submitted code to ensure it works with C++98 on the lab computers. The results of the function will stay the same however.}

\subsection{Requirement 2: Light and material properties}

As mentioned above, the author implemented tessellation on flat planes (triangular and rectangular) to allow for better lighting to be recognised (\textit{Figure 8}). This works by allowing light to be more evenly diffused across a plane by splitting it up into smaller sub-planes, each with their own diffusive and specular properties. \textit{Figure 9} shows a demonstration of tessellation working on a triangular plane using the function in \textit{Figure 8}. You can clearly see how diffusive and speculative light patterns can be recognised through this implementation.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.6\linewidth}
    \includegraphics[width=\linewidth]{img/triangle_tesselation_implementation.png}
  \end{subfigure}
  
  Figure 9: Triangle tessellation demonstration 
\end{figure}



\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.45\linewidth}
    \includegraphics[width=\linewidth]{img/materials_implementation.png}
  \end{subfigure}
  
  Figure 10: Material property definitions
\end{figure}

Material properties were also chosen to allow for diffusive and speculative light properties to be recognised. \textit{Figure 10} shows the implementation of these material properties. These are constructed from the \textit{materialStruct} defined in \textit{Figure 7} which contains 4 attributes for ambient, diffusive, specular and shininess properties of the material. For example, we can see the \textit{greenShinyMaterial} has a shininess rating of 100 and all the speculative properties set to 1.0. We can clearly see this demonstrated in \textit{Figure 9} which is using the \textit{greenShinyMaterial}. To contrast, \textit{Figure 4} shows an example of \textit{redMaterials} on the barn which has a shininess rating of 0.0 and no specular properties, hence why the building looks 'flat' and not shiny. The design theory behind this is that buildings do not reflect light, especially a barn made out of wood.

\section{Band 2: 50\% \-- 60\%}

The second requirement band builds upon band 1 by allowing for users (who will be viewing the scene) to interact with components within the scene and manipulate properties. The requirements for band 2 are as follows:

\begin{itemize}
	\item The scene must contain at least one element of user interaction.
\end{itemize}

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.7\linewidth}
    \includegraphics[width=\linewidth]{img/interaction_implementation.png}
  \end{subfigure}
  
  Figure 11: World interaction controls
\end{figure}

The author designed and implemented the interactive components of their scene using QT objects. \textit{Figure 11} shows some of the interactive components included within the scene, such as sliders and check-boxes. These allow the user to manipulate world properties such as zooming in or rotating the scene, or changing lighting properties by setting the quality of objects and enabling / disabling smooth lighting. 

The author designed the interaction to be this way, that is allowing for the manipulation of the 'camera' and lighting because, not only did it meet the above requirements, but it also made it easier to develop the scene. The author could change the 'camera' angles and zoom to get a better look of object instances being created in the scene.

\newpage
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.7\linewidth}
    \includegraphics[width=\linewidth]{img/interaction_code.png}
  \end{subfigure}
  
  Figure 12: World interaction implementation
\end{figure}

\textit{Figure 12} shows the implementation of the interactive components shown in \textit{Figure 11}. Each component is a QT object such as \textit{QLabel} or \textit{QSlider} and are grouped by a \textit{QGroupBox} to make it explicit that these are the world controls. \textit{Figure 13} shows the SLOT implementation that connects these controls to the relative methods in the \textit{sceneWidget} object (that contains the scene), facilitating the user interaction.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{img/slot_imlementation.png}
  \end{subfigure}
  
  Figure 13: World interaction SLOT implementation
\end{figure}

\textit{Figure 14} shows the implementation of these SLOTS connected to the interactive components; they are just simple methods that update attributes in the \textit{sceneWidget} class to facilitate user interaction. For example, the \textit{zoomSlider} is a \textit{QSlider} object demonstrated in \textit{Figure 12}. This is connected to the \textit{updateZoomRange} method in \textit{Figure 13}, passing through an int of the sliders current value. This value is then used to set the \textit{zoom} attribute in \textit{Figure 14} which in turn can be passed to \textit{glScalef(zoom, zoom, zoom)} to 'zoom' in the camera, demonstrating the simple capacity for user interaction.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.9\linewidth}
    \includegraphics[width=\linewidth]{img/interaction_update.png}
  \end{subfigure}
  
  Figure 14: World interaction method implementation
\end{figure}

\section{Band 3: 60\% \-- 70\%}

The third band defines 3 new requirements to increase the complexity and design of the scene. The requirements for band 3 are as follows:

\begin{itemize}
	\item The scene must contain an element of animation. 
	\item The scene must contain at least one convex object that you have constructed from polygons.
	\item The scene must contain texture mapping and use all the textures provided.
\end{itemize}

\subsection{Requirement 1: animation}
We have already seen in \textit{Figure 12} the implementation of a \textit{QTimer} object that connects to the \textit{updateAngle} method to increment an angle attribute that is used for animation throughout the scene (\textit{Firures 13, 14}). 

\subsubsection{Design}
The author designed the scene to originally include a car that would drive up and down the road, fulfilling the animation requirement. Ultimately, this would change to a pig that would walk in a circular motion through the scene (\textit{Figure 6}) to meet the authors design of creating a farm. An additional animated component was designed later through the development of the scene, to ensure this requirement was fully met. This included a waving sign that would hang from the side of the barn (\textit{Figure 15}). The author wanted to make the farm seem 'alive' and having a sign rock back and forth by what could presumed to be wind would help convey this. 

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[width=\linewidth]{img/sign_design.png}
  \end{subfigure}
  
  Figure 15: Waving sign instance design
\end{figure}

\subsubsection{Implementation}

As mentioned, an angle attribute was used to facilitate all the animations throughout the scene. The attribute will be passed to \textit{glRotatef} and \textit{glTranslatef} function calls. When the attribute is updated we repaint the scene which recalls all these functions and re-draws the animated component in a different position (\textit{Figure 14}). When this is done over-and-over in a loop we create an animation!

\textit{Figure 16} demonstrates the implementation of this type of animation on the waving sign previously discussed. The angle attribute is used to calculate a sine wave $y(x) = \sin(x)$ which is then passed to a call to \textit{gfRotatef}, rotating the sign in the $x$ axis. We set the upper limit of the sine wave to $45\deg$ to allow the sign to ossolate between $-45\deg$ and $45\deg$. The speed of the animation is set to $0.1$ since the author felt this looked best; increasing this value speeds up the animation and visa versa.

The second instance of animation in the scene, being the pig that walking in a circle, will be discussed at a later section in band 4 when hierarchical modelling is covered.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.8\linewidth}
    \includegraphics[width=\linewidth]{img/sign_implementation.png}
  \end{subfigure}
  
  Figure 16: Waving sign implementation
\end{figure}

\subsection{Requirement 2: convex object}

By definition, an object is convex if each of its interior angles are less than or equal to $180\deg$. This therefore includes the likes of simple objects such as rectangles, triangles, cubes, tetrahedrons and cylinders. We already know from our discussions in band 1 that all of these shapes have been implemented within the scene, therefore we can deduce that this requirement has already been met.

Going back to the example given in \textit{Figure 8}, the author also had to ensure the normals and texture coordinates of each of these convex objects were correctly implemented. We can see from this example the author is using \textit{glNormal3f} and \textit{glTexCoord2f} to set the normals and texture coordinates to the same values as the vertex positions. \textit{Figure 17} demonstrates this implementation on a convex tetrahedron. We see the textures correctly show along with the specular and diffusive lighting properties. If the normals had not been implemented correctly the texture would show as black, therefore we know these work.

\newpage

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.55\linewidth}
    \includegraphics[width=\linewidth]{img/convex_tet_implementation.png}
  \end{subfigure}
  
  Figure 17: Convex tetrahedron (rotated) normal and texture demonstration
\end{figure}

\textit{Figure 18} shows the implementation for a convex tetrahedron made out of triangular planes as an example of how the author has implemented a convex object. We see the tetrahedron is made up of 4 triangle planes, the first for the bottom of the tetrahedron and the other 3 individually translated and rotated around the origin of the tetrahedron. These planes are than angled inwards towards the origin to create the triangular effect, completing the tetrahedron. 

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.9\linewidth}
    \includegraphics[width=\linewidth]{img/tetrahedron_implementation.png}
  \end{subfigure}
  
  Figure 18: Convex tetrahedron implementation
\end{figure}

\newpage
\subsection{Requirement 3: texture mapping}

We have already seen throughout the report glimpses of texture mapping implemented within the scene. The author will now discuss the design and implementation of this texture mapping to meet the requirements specified above.

\subsubsection{Design}

\begin{figure}[h!]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{img/marc_implementation.png}
  \label{fig:sub1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{img/markus_implementation.png}
  \label{fig:sub2}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1.0\linewidth]{img/earth_implementation.png}
  \label{fig:sub2}
\end{subfigure}
\caption{\textit{textures.tar} texture design in scene}  
\label{fig:test}
\end{figure}


One of the main requirements of the texture mapping implementation was to include the provided textures in the \textit{textures.tar} file. This included 3 files:
	
\begin{itemize}
	\item Marc\_Dekamps.ppm
	\item markus.ppm
	\item Mercator\--projection.ppm
\end{itemize}

\textit{Figure 19} shows the authors design on how to implement the provided textures. For the pictures of both of the lectures: \textit{Mark\_Dekamps.ppm} and \textit{markus.ppm}, the author intends to create billboard style signs along the side of the road to face with the lecturers images. The original idea here was to add text overlapping these textures which could say gags such as "have you seen this man?", creating the appearance of wanted posters of the lecturers. In the end however, this was not achieved since the author could not make sense, nor have the time, of how to apply fonts within an OpenGL scene and settled with the approach in \textit{Figure 19}.

To make use of the \textit{Mercator\--projection.ppm} file, the author intends to create a picture frame on the side of one of the buildings in the scene to face with the texture. The design decision behind this was to simply find a place to house the texture, since it did not fit the scene very well. Other approaches here could have been to create a sphere 'planet' in the background that could be faced with the texture. 


\begin{figure}[htb]
    \centering % <-- added
\begin{subfigure}{0.25\textwidth}
  \includegraphics[width=\linewidth]{../textures/barnWall.jpg}
  \label{fig:1}
\end{subfigure}\hfil % <-- added
\begin{subfigure}{0.25\textwidth}
  \includegraphics[width=\linewidth]{../textures/bush.jpg}
  \label{fig:2}
\end{subfigure}\hfil % <-- added
\begin{subfigure}{0.25\textwidth}
  \includegraphics[width=\linewidth]{../textures/grass.jpg}
  \label{fig:3}
\end{subfigure}

\medskip
\begin{subfigure}{0.25\textwidth}
  \includegraphics[width=\linewidth]{../textures/metalRoof.jpg}
  \label{fig:4}
\end{subfigure}\hfil % <-- added
\begin{subfigure}{0.25\textwidth}
  \includegraphics[width=\linewidth]{../textures/road.jpg}
  \label{fig:5}
\end{subfigure}\hfil % <-- added
\begin{subfigure}{0.25\textwidth}
  \includegraphics[width=\linewidth]{../textures/roofTile.jpg}
  \label{fig:6}
\end{subfigure}

\medskip
\begin{subfigure}{0.25\textwidth}
  \rotatebox[origin=c]{180}{\includegraphics[width=\linewidth]{../textures/barnSign.jpg}}
  \label{fig:4}
\end{subfigure}\hfil % <-- added
\begin{subfigure}{0.25\textwidth}
  \includegraphics[width=\linewidth]{../textures/barnWallDoor.jpg}
  \label{fig:5}
\end{subfigure}\hfil % <-- added
\begin{subfigure}{0.25\textwidth}
  \includegraphics[width=\linewidth]{../textures/metalDoor.jpg}
  \label{fig:6}
\end{subfigure}
\caption{Additional textures used within the scene}
\label{fig:images}
\end{figure}

\textit{Figure 20} shows additional textures designed to be used within the scene, this includes grass for the ground, bush for the bushes, wall textures and roof textures. The author can add more detail to the scene by creating copies of some of these textures and adding additional elements such as doors, windows etc. This was achieved for the barn we see in \textit{Figure 4}.

\subsubsection{Implementation}

A texture class was implemented to facilitate texturing of all objects within the scene. This included attributes such as the width and height of the texture, the texture data (that holds the actual image), as well as methods for loading the texture from a file. \textit{Figure 21} shows this implementation. The class was based on an \textit{Image} class provided in previous COMP3811 Computer Graphics tutorials, but has been modified to better suit the needs of the author and the scene they are trying to create. 

The constructor shown in \textit{Figure 21} handles loading an image from a filename using the QT \textit{QImage} class, which we can automatically grab the width and height of the image from as attributes. A \textit{GLubyte} is used to store the image data, with the size being the size of the image $* 3$ for each of the individual RGB pixel values. We then simply loop over each of the pixels in the image and calculate the location of each RGB value, which we set in our \textit{GLubyte} attribute.

A previous method of loading textures was tried (commented out in \textit{Figure 21}), using the \textit{stb\_image.h} header file (\href{https://github.com/nothings/stb/blob/master/stb_image.h}{\textbf{stb\_image.h github}}) which provides functions such as \textit{stbi\_load} returning an unsigned char* of image data and the width and height. This was ultimately chosen not to be used since the current method allows for more flexibility in assigning pixel values, which can allow for more control over inverted colours and different contrasts.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{img/texture_code.png}
  \end{subfigure}
  
  Figure 21: Texture class implementation
\end{figure}


Textures where applied to objects by enabling and disabling GL\_TEXTURE\_2D for each objects to be textured using the \textit{glEnable(GL\_TEXTURE\_2D)} and \textit{glDisable(GL\_TEXTURE\_2D)} functions (\textit{Figure 22}). \textit{glTexImage2D} was then used to load the actual image data and apply it to the object. GL\_RGB was used over GL\_RGBA since no alpha values where needed within the scene. Several of the same shape functions shown in \textit{Figure 7} were declared to allow for the choice between texturing or no texturing to take place.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{img/texture_implementation.png}
  \end{subfigure}
  
  Figure 22: Texturing objects
\end{figure}

As previously discussed in section 4.2, texture coordinates of each object also had to be correctly implemented to allow for proper texture mapping to take place (shown in \textit{Figure 17)}. \textit{Figure 8} shows an example of this on a triangular plane using \textit{glTexCoord2f}. For each object: rectangle, triangle, cube, tetrahedron and cylinder, each of the texture coordinates where simply set to the same position as the vertex coordinates.

\section{Band 4: 70\% \-- 100\%}

The final band builds upon all the tools we have already learnt and implemented, to create the final scene we see in \textit{Figure 1}. The requirements for band 4 are as follows:

\begin{itemize}
	\item Contain an object that requires hierarchical modelling and display motion in some of its parts - moves in a circular motion through the scene.
	\item Use various elements of user interaction to set the radius or speed of the object.
\end{itemize}

\subsection{Requirement 1: hierarchical modelling}

Earlier in this report we looked at \textit{Figure 6}; a fence used to create a pig penn. The pig shown here is our hierarchically constructed model made out of 4 separate components: head, body, legs and back, with individual animation applied to each component. The author chose to construct a pig as their hierarchical model since it best fit the theme of the farm, although any farm animal such as cows, sheep etc. could have been chosen here.

The author wrote a pig class to facilitate all functionality of the hierarchical model including the separate components. \textit{Figure 23} shows the header file implementation of this class (without method comments since these would not fit in the screenshot - please look at the code for an extensive description of what each function does). 2 attributes \textit{bodyLimit} and \textit{legLimit} allowed for motion to take place in separate parts of the model, namely the body and legs, head and back. A \textit{speed} attribute is used to set the walking pace of the pig in a circular motion in the pig penn.
 
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.65\linewidth}
    \includegraphics[width=\linewidth]{img/pig_code.png}
  \end{subfigure}
  
  Figure 23: Pig class implementation
\end{figure}

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.7\linewidth}
    \includegraphics[width=\linewidth]{img/pig_implementation.png}
  \end{subfigure}
  
  Figure 24: Hierarchical pig implementation
\end{figure}

These separate parts of the model are then 'put together' in a \textit{piggy} method in our scene (\textit{Figure 24}) translating and scaling where necessary to ensure each component is in the right place. The final result is the pig model we see in \textit{Figure 6}! To make the pig walk in a circular motion through the scene, we call our \textit{piggy} method after translating the pig around the radius in both the $x$ and $-y$ axis. We also rotate the pig $45\deg$ in the $z$ axis, the same direction the pig is walking, to make it follow the curve of the circle. We do one final rotation beforehand with our \textit{angle} attribute which creates the animation, since this value is incremented indefinitely. This angle is scaled by the speed of the pig so the rotation remains linear when the speed is increased (\textit{Figure 25}). 

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.7\linewidth}
    \includegraphics[width=\linewidth]{img/pig_rotate_implementation.png}
  \end{subfigure}
  
  Figure 25: Hierarchical pig circular movement implementation
\end{figure}

Many other instances of hierarchical modelling were implemented into the final scene, such as when constructing the buildings, silo, fence, or any of the 3d convex shapes such as cube and tetrahedron. The author found hierarchical modelling exceptionally useful throughout the implementation of their scene and is one of the main takeaways of the project! 

\subsection{Requirement 2: User interaction}

We have already discussed in section 3 user interactive that was incorporated to allow world properties such as lighting, rotation and zoom to be changed. The author expands upon these implementations to meet the requirements for band 4. Namely, interaction to allow the user to control the radius and speed of the hierarchical pig previously discussed was implemented.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.35\linewidth}
    \includegraphics[width=\linewidth]{img/pig_controls.png}
  \end{subfigure}
  
  Figure 26: Hierarchical pig controls
\end{figure}

\textit{Figure 26} shows the design of these interactive controls. The author is using spin boxes instead of sliders here to allow the user to easily get the exact integer value for the speed and radius they want to set. \textit{Figure 27} shows the SLOT implementations connected to the spin boxes. As with \textit{Figure 14} we simply update the value of the speed and radius to the value from the spin box (in this case $/ 10$ to match the current scale). In \textit{updatePigSpeed} we also set the speed of our pig class object so that changes are reflected in the hierarchical models animation. The \textit{pigRadius} value is used within our method in \textit{Figure 25}.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{img/pig_interaction_update.png}
  \end{subfigure}
  
  Figure 27: Hierarchical pig interaction method implementation
\end{figure}

\newpage

\section{Conclusion}
To conclude, the author feels they have met all of the requirements for each band 1, 2, 3, 4 at a reasonable level. Going into the project, the author had no prior experience with OpenGL or graphical modelling so found a sharp learning curve getting to grips with the basics of matrix transformations and thinking about placements in a 3 dimensional space. The further the author got with the project however, the more comfortable they felt with OpenGL as if the learning curve was exponential - once past the initial phase, learning and implementation became much easier. 

The author leaves the scene shown in \textit{Figure 1} and is exited to work with OpenGL again, maybe perhaps the 'new' OpenGL and even game development!

\section{Remarks}

\subsection{Code differences}
The author would like to add that some of the code provided in this report will differ from the final code submitted. These changes are purely syntactical and do not reflect and changes in implementation. The changed were made to reflect the University lab computers running C++98 by default, whereas the author implemented their scene in C++11. Given the changes, the scene will run on the C++98 lab computers without any extra compilation parameters.

\subsection{Running the code}
To run the code on a University of Leeds lab machine, or any Linux device, enter the following simple commands:

\begin{itemize}
	\item \textit{qmake} - If the error 'command not found...' is issued run the command \textit{module add qt} and rerun.
	
	\item \textit{make} - This may show some warnings.
	
	\item \textit{./Scene} 
\end{itemize}

\subsection{Video demonstration}
In the case that the assessor cannot run the code submitted the author has provided a video demonstration. This demonstration is provided in a .mkv format and is available under: \textbf{video demo/demonstration.mkv}.

\end{document}