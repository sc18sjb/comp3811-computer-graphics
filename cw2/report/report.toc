\contentsline {section}{\numberline {1}Introduction}{2}{section.1}% 
\contentsline {subsection}{\numberline {1.1}Overview}{2}{subsection.1.1}% 
\contentsline {section}{\numberline {2}Band 1: 40\% \-- 50\%}{3}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Scene design}{3}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Requirement 1: complexity through instancing}{4}{subsection.2.2}% 
\contentsline {subsubsection}{\numberline {2.2.1}Design}{5}{subsubsection.2.2.1}% 
\contentsline {subsubsection}{\numberline {2.2.2}Design intentions}{6}{subsubsection.2.2.2}% 
\contentsline {subsubsection}{\numberline {2.2.3}Implementation}{8}{subsubsection.2.2.3}% 
\contentsline {subsection}{\numberline {2.3}Requirement 2: Light and material properties}{10}{subsection.2.3}% 
\contentsline {section}{\numberline {3}Band 2: 50\% \-- 60\%}{12}{section.3}% 
\contentsline {section}{\numberline {4}Band 3: 60\% \-- 70\%}{14}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Requirement 1: animation}{14}{subsection.4.1}% 
\contentsline {subsubsection}{\numberline {4.1.1}Design}{15}{subsubsection.4.1.1}% 
\contentsline {subsubsection}{\numberline {4.1.2}Implementation}{15}{subsubsection.4.1.2}% 
\contentsline {subsection}{\numberline {4.2}Requirement 2: convex object}{16}{subsection.4.2}% 
\contentsline {subsection}{\numberline {4.3}Requirement 3: texture mapping}{18}{subsection.4.3}% 
\contentsline {subsubsection}{\numberline {4.3.1}Design}{18}{subsubsection.4.3.1}% 
\contentsline {subsubsection}{\numberline {4.3.2}Implementation}{20}{subsubsection.4.3.2}% 
\contentsline {section}{\numberline {5}Band 4: 70\% \-- 100\%}{21}{section.5}% 
\contentsline {subsection}{\numberline {5.1}Requirement 1: hierarchical modelling}{21}{subsection.5.1}% 
\contentsline {subsection}{\numberline {5.2}Requirement 2: User interaction}{23}{subsection.5.2}% 
\contentsline {section}{\numberline {6}Conclusion}{25}{section.6}% 
\contentsline {section}{\numberline {7}Remarks}{25}{section.7}% 
\contentsline {subsection}{\numberline {7.1}Code differences}{25}{subsection.7.1}% 
\contentsline {subsection}{\numberline {7.2}Running the code}{25}{subsection.7.2}% 
\contentsline {subsection}{\numberline {7.3}Video demonstration}{25}{subsection.7.3}% 
