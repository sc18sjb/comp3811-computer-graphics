#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <QImage>
#include "utils/Shapes.h"
#include "utils/Texture.h"
#include "utils/Pig.h"

struct materialStruct;

class SceneWidget: public QGLWidget {
    Q_OBJECT // Define as an object to allow for slots.

    public:
        /**
         * Method SceneWidget
         * Constructor to set attribute default values.
         * 
         * PARAMS:  parent: The QWidget parent object to inherit from.
         * */
        SceneWidget(QWidget *parent);

        /**
         * Method ~SceneWidget
         * Destructor to clean up all the attributes.
         * */
        ~SceneWidget();

        /**
         * Method getZoom (getter)
         * 
         * RETURNS: The current world zoom value.
         * */
        float getZoom() {
            return zoom;
        }

        /**
         * Method getRotate (getter)
         * 
         * RETURNS: The current world rotation value.
         * */
        float getRotate() {
            return rotate;
        }

        /**
         * Method getQuality (getter)
         * 
         * RETURNS: The current world quality value.
         * */
        int getQuality() {
            return quality;
        }

        /**
         * Method getPigRadius (getter)
         * 
         * RETURNS: The current pig walking radius value.
         * */
        float getPigRadius() {
            return pigRadius;
        }

        /**
         * Method getPigSpeed (getter)
         * 
         * RETURNS: The current pig walking speed value.
         * */
        float getPigSpeed() {
            return pigSpeed;
        }

    public slots:
        /**
         * Method updateRotateRange
         * update the world rotation value.
         * 
         * PARAMS:  value:  The value obtained from the UI slider.
         * */
        void updateRotateRange(int value);

        /**
         * Method updateZoomRange
         * update the world zoom value.
         * 
         * PARAMS:  value:  The value obtained from the UI slider.
         * */
        void updateZoomRange(int value);

        /**
         * Method updateQuality
         * Update the world quality value.
         * 
         * PARAMS:  value:  The value obtained from the UI slider.
         * */
        void updateQuality(int value);

        /**
         * Method setSmoothLighting
         * Update the world smooth lightint value (bool).
         * 
         * PARAMS:  state:  The value obtained from the UI checkbox.
         * */
        void setSmoothLighting(int state);

        /**
         * Method updateAngle
         * Increment the animation angle value.
         * */
        void updateAngle();

        /**
         * Method updatePigRadius
         * Update the pig walking radius value.
         * 
         * PARAMS:  value:  The value obtained from the UI spinbox.
         * */
        void updatePigRadius(int value);

        /**
         * Method updatePigSpeed
         * Update the pig walking speed value.
         * 
         * PARAMS:  value:  The value obtained from the UI slider.
         * */
        void updatePigSpeed(int value);

    protected:
        /**
         * Method initializeGL (override)
         * Initialize the OpenGL instance and set all the default values.
         * */
        void initializeGL();

        /**
         * Method redizeGL (override)
         * Resize the OpenGL instance and set the display propterties.
         * */
        void resizeGL(int w, int h);

        /**
         * Method paintGL (override)
         * Draw the OpenGL instance and overthing on the screen.
         * */
        void paintGL();

        /**
         * Method house
         * Draw a house out of cubes and set the cube texture properties.
         * 
         * PARAMS:  w:              The width scaler for the house.
         *          h:              The height scaler for the house.
         *          d:              The depth scaler for the house.
         *          wallMaterials:  An array containing material properties for each face of the walls constructing the house.
         *          roofMaterials:  An array containing material properties for each face of the roof constructing the house.
         *          wallTextures:   An array containing texture class objects for each face of the walls construcing the house.
         *          roofTextures:   An array containing texture class objects for each face of the roof constructing the house.
         * */
        void house(float w, float h, float d, MaterialVec wallMaterials, MaterialVec roofMaterials, TextureVec wallTextures, TextureVec roofTextures);
        
        /**
         * Method ground
         * Draw the ground plane of the scene and textures on the ground.
         * */
        void ground();

        /**
         * Method bush
         * Draw a bush constructed out of tesselated tetrahedrons.
         * */
        void bush();

        /**
         * Method silo
         * Draw a silo contructed out of a convex cylinder and a GLU sphere object.
         * */
        void silo();

        /**
         * Method fence
         * Draw a fence partition to construct a longer fence.
         * 
         * PARAMS:  partitions: The number of fence paritions to draw.
         * */
        void fence(int partitians);

        /**
         * Method sign
         * Draw a road billboard style sign with a texture face applied.
         * 
         * PARAMS:  texture: A texture class object containing image data and width & height.
         * */
        void sign(Texture* texture);

        /**
         * Method wavingSign
         * Draw an animated 'pub' style waving sign with a texture face applied.
         * 
         * PARAMS:  texture: A texture class object containing image data and width & height.
         *          angle:   The incrementing angle attribute.
         * */
        void wavingSign(Texture* texture, double angle);
        
        /**
         * Method pictureFrame
         * Draw a picture frame with a picture face texture applied.
         * 
         * PARAMS:  texture:    A texture class object containing image data and width & height.
         * */
        void pictureFrame(Texture* texture);

        /**
         * Method piggy
         * Draw an animated heirarchicle pig using the pig class instance.
         * */
        void piggy();

        /**
         * Method scene
         * Create everything in the scene, being a combination of all the above draw methods.
         * */
        void scene();


    private:        
        // A map to hold all the textures relating to a name so that they can be easily accessed
        TextureMap textureMap;
        
        // A pig class instance for creating a hierarchicle pig
        Pig* pig;

        float zoom;         // World zoom amount
        float rotate;       // World rotation angle
        float angle;        // Animations rotation angle
        float pigRadius;    // Radius of the circle the pig walks around in
        float pigSpeed;     // Speed of the pig walking around in the circle
        int quality;        // Quality of objects and the shadows and reflections
        int smoothLighting; // Bool to enable / disable smooth lighting
};

#endif