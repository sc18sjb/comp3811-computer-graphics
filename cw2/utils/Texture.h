#ifndef _TEXTURE_
#define _TEXTURE_

#include<string>
#include <QImage>
#include <GL/glu.h>


class Texture {
    public:
        /**
         * Method Texture
         * Set the image data from a provided image file.
         * 
         * PARAMS:  fileName:   The image file.
         * */
        Texture(const std::string& fileName);

        /**
         * Method ~Texture
         * Destructor to clean up the image data.
         * */
        ~Texture();

        /**
         * Method getWidth (getter)
         * 
         * RETURNS: The width of the image file.
         * */
        unsigned int getWidth() const { 
            return width;
        }

        /**
         * Method getHeight (getter)
         * 
         * RETURNS: The height of the image file.
         * */
        unsigned int getHeight() const { 
            return height;
        }

        /**
         * Method getTexture (getter)
         * 
         * RETURNS: The GLubyte texture image data.
         * */
        const GLubyte* getTexture() const {
            return image;
        }
    
    private:
        unsigned int width;     // Width of the image data.
        unsigned int height;    // Height of the image data.

        QImage* qImage;         // QImage object containing the image data.
        GLubyte* image;         // Stores the image individual pixel data.
};

#endif
