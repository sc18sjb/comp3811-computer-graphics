#ifndef __SHAPES__
#define __SHAPES__
#include <vector>
#include <map>
#include <string>
#include "Texture.h"

// Struct to define lighting values.
typedef struct materialStruct {
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat shininess;
} materialStruct;

// Defining the types used throughout the program.
typedef std::vector<materialStruct> MaterialVec;    // Array of materials.
typedef std::vector<Texture*> TextureVec;           // Array of textures.
typedef std::map<std::string, Texture*> TextureMap; // Map of textures with a string name.
typedef std::vector<float> Vertex;                  // Vertex comprising of 2 floats.
typedef std::vector<Vertex> Triangle;               // A triangle made up of 3 vertices.
typedef std::vector<Triangle> TriangleVec;          // Array of triangles.

/**
 * Function setMaterialProperties
 * Set the ambient, diffuse, specular and shininess of an object
 * using material properties.
 * 
 * PARAMS: material:    Material containing properties.
 * */
void setMaterialProperties(const materialStruct* material);


/**
 * Function rectangle
 * Draw a tesellated rectangle of size 1x1.
 * 
 * PARAMS:  material:       Material containing properties.
 *          subDivisions:   Number of individual squares to make up the rectangle (tesselation).
 * */
void rectangle(const materialStruct* material, int subDivisions);

/**
 * Function rectangle
 * Draw a tesselated rectangle with textures applied to the face.
 * 
 * PARAMS:  material:       Material containing properties.
 *          texture:        Texture class containing image data, width & height.
 *          subDivisions:   Number of individual squares to make up the rectangle (tesselation). 
 * */
void rectangle(const materialStruct* material, Texture* texture, int subDivisions);

/**
 * Function subDivideTriangle
 * Take a triangle and subdivide it into 4 smaller triangles from
 * the midpoint of each line AB, BC, CA.
 * 
 * PARAMS:  A:  First vertice of the triangle.
 *          B:  Second vertice of the triangle.
 *          C:  Third veritce of the triangle.
 *          n:  The 'depth' of the subdivision (how many n times do we subdivide).
 * 
 * RETURNS: An array of triangle cordinates defining each of the subtriangles. 
 * */
TriangleVec subDivideTriangle(Vertex A, Vertex B, Vertex C, int n);

/**
 * Function triangle
 * Draw a tesselated triangle of size 1x1.
 * 
 * PARAMS:  material:       Material containing properties.
 *          subDivisions:   The 'depth' of the triangle; i.e. how many times we break it up into 4 smaller triangles.
 * */
void triangle(const materialStruct* material, int subDivisions);

/**
 * Function triangle
 * Draw a tesselated traingle with textures applied to the face.
 * 
 * PARAMS:  material:       Material containing properties.
 *          texture:        Texture class containing image data, width & height.
 *          subDivisions:   The 'depth' of the triangle; i.e. how many times we break it up into 4 smaller triangles.
 * */
void triangle(const materialStruct* material, Texture* texture, int subDivisions);

/**
 * Function decideTextureTriangle
 * Given an array of textures, decide whether to texture a triangle face or not based on if the array is empty.
 * 
 * PARAMS:  materials:      An array of material properties.
 *          textures:       An array of texture class objects.
 *          subDivisions:   The 'depth' of the triangle; i.e. how many times we break it up into 4 smaller triangles.
 *          n:              The index of the materials & textures arrays.
 * */
void decideTextureTriangle(MaterialVec materials, TextureVec textures, int subDivisions, int n);

/**
 * Function tetrahedron
 * Draw a tesselated tetrahedron constructed out of triangle faces
 * 
 * PARAMS:  materials:      An array of material properties for each face of the tetrahedron.
 *          subDivisions:   The 'depth' of each triangle face; i.e. how many times we break it up into 4 smaller triangles.
 *          textures:       An array of texture class objects for each face of the tetrahedron.
 * */
void tetrahedron(MaterialVec materials, int subDivisions, TextureVec textures = TextureVec ());

/**
 * Function decideTextureRectangle
 * Given an array of textures, decide whether to texture a rectangle face or not based on if the array is emoty.
 * 
 * PARAMS:  materials:      An array of material properties.
 *          textures:       An array of texture class objects.
 *          subDivisions:   Number of individual squares to make up the rectangle (tesselation). 
 *          n:              The index of the materials & textures arrays.
 * */
void decideTextureRectangle(MaterialVec materials, TextureVec textures, int subDivisions, int n);

/**
 * Function cube
 * Draw a tesselated cube constructed out of rectangle faces.
 * 
 * PARAMS:  materials:      An array of material properties for each face of the cube.
 *          subDivisions:   Number of individual squares to make up each rectangle face (tesselation).
 *          textures:       An array of texture class objects for each face of the cube. 
 * */
void cube(MaterialVec materials, int subDivisions, TextureVec textures = TextureVec ());

/**
 * Function cylinder
 * Draw a convex cylinder.
 * 
 * PARAMS:  material:           Material containing properties.
 *          faces:              The number of faces to construct the cylinder (round).
 *          heightDivisions:    The number of height divisions to construct the cylinder.
 * */
void cylinder(const materialStruct* material, int faces, int heightDivisions);

/**
 * Function cylinder
 * Draw a convex cylinder with textures applied to each face constructing the cylinder.
 * 
 * PARAMS:  material:           Material containing properties to be applied to ALL faces of the cylinder.
 *          texture:            Texture class object containing image data, width & height to be applied to ALL faces of the cylinder.
 *          faces:              The number of faces to construct the cylinder (round).
 *          heightDivisions:    The number of height divisions to construct the cylinder.
 * */
void cylinder(const materialStruct* material, Texture* texture, int faces, int heightDivisions);

/**
 * Function GLUsphere
 * Use the gluSphere method to create a sphere object.
 * 
 * PARAMS:  material:   Material containing properties.
 *          slices:     The number of horizontal slices to construct the sphere.
 *          stacks:     The number of vertical stacks to construct the sphere.
 * */
void GLUsphere(const materialStruct* material, int slices, int stacks);

/**
 * Function GLUsphere
 * Use the gluSPhere method to create a sphere object with textures applied.
 * 
 * PARAMS:  material:   Material containing properties.
 *          texture:    Texture class object containing image data, width & height.
 *          slices:     The number of horizontal slices to construct the sphere.
 *          stacks:     The number of vertical stacks to construct the sphere.
 * */
void GLUsphere(const materialStruct* material, Texture* texture, int slices, int stacks);

#endif