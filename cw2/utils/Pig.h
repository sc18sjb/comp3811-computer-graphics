#ifndef __PIG__
#define __PIG__

class Pig {
    public:
        /**
         * Method Pig
         * Constructor to set attribute default values.
         * 
         * PARAMS:  bodyLimit:  The pig body angle upper limit.
         *          legLimit:   The pig leg angle upper limit.
         *          pigSpeed:   The speed the pig will move at.
         * */
        Pig(double bodyLimit, double legLimit, float pigSpeed);

        /**
         * Method head
         * Create the pigs head and ossolate it from side to side.
         * 
         * PARAMS:  time: Used to calculate the angle of ossolation in a sine wave.
         * */
        void head(double time);

        /**
         * Method leg
         * Create the pigs leg and ossolate them up and down.
         * 
         * PARAMS:  time: Used to calculate the angle of ossolation in a sine wave.
         * */
        void leg(double limit, double time);
        
        /**
         * Method body
         * Create the pigs body and ossolate the body side to side.
         * 
         * PARAMS:  time:   Used to calculate the angle of ossolation in a sine wave.
         * */
        void body(double time);

        /**
         * Method back
         * Create the pigs back and ossolate the tail up and down.
         * 
         * PARAMS:  time:   Used to calculate the angle of ossolation in a sine wave.
         * */
        void back(double time);

        /**
         * Method setQuality (setter)
         * Set the pigs quality different to the default value.
         * 
         * PARAMS:  value:  The value of the pigs quality.
         * */
        void setQuality(float value) {
            quality = value;
        }

        /**
         * Method setSpeed (setter)
         * Set the pigs speed different to the initial value.
         * 
         * PARAMS:  value:  The value of the pigs speed.
         * */
        void setSpeed(float value) {
            speed = value;
        }

    protected:
        /**
         * Method calculateBodyAngle
         * Calculate the position of the body in the sine wave.
         * 
         * PARAMS:  time:   The current time set in the sine wave.
         *          limit:  The upper limit of the sine wave.
         * 
         * RETURNS: The calculated body angle.
         * */
        double calculateBodyAngle(double time, double limit = -1);

        /**
         * Method calculateLegAngle
         * Calculate the position of the legs in the sine wave.
         * 
         * PARAMS:  time:   The current time set in the sine wave.
         *          limit:  The upper limit of the sine wave.
         * 
         * RETURNS: The calculate leg angle.
         * */
        double calculateLegAngle(double time, double limit = -1);

    private:
        double bodyLimit;   // Body upper limit
        double legLimit;    // Leg upper limit
        float speed;        // Pig walking speed
        int quality;        // Pig lightint and tesselation quality
};

#endif