#ifndef __MATERIALS__
#define __MATERIALS__
#include "Shapes.h"

static materialStruct greenShinyMaterials = {
    { 0.494, 0.784, 0.313, 1.0},
    { 0.494, 0.784, 0.313, 1.0},
    { 1.0, 1.0, 1.0, 1.0},
    100.0
};

static materialStruct brownShinyMaterials = {
    { 0.305, 0.207, 0.141, 1.0},
    { 0.305, 0.207, 0.141, 1.0},
    { 1.0, 1.0, 1.0, 1.0},
    100.0
};

static materialStruct brickShinyMaterials = {
    { 0.717, 0.329, 0.176, 1.0},
    { 0.717, 0.329, 0.176, 1.0},
    { 0.0, 0.0, 0.0, 0.0},
    100.0
};

static materialStruct whiteShinyMaterials = {
    { 1.0, 1.0, 1.0, 1.0},
    { 1.0, 1.0, 1.0, 1.0},
    { 1.0, 1.0, 1.0, 1.0},
    100.0
};

static materialStruct greyShinyMaterials = {
  { 0.333, 0.333, 0.333, 1.0},
  { 0.333, 0.333, 0.333, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0
};

static materialStruct pinkShinyMaterials = {
  { 0.996, 0.498, 0.611, 1.0},
  { 0.996, 0.498, 0.611, 1.0},
  { 0.0, 0.0, 0.0, 0.0},
  100.0
};

static materialStruct greyMaterials = {
    { 0.333, 0.333, 0.333, 1.0},
    { 0.333, 0.333, 0.333, 1.0},
    { 0.0, 0.0, 0.0, 0.0},
    0.0
};

static materialStruct redMaterials = {
    { 0.486, 0.039, 0.007, 1.0},
    { 0.486, 0.039, 0.007, 1.0},
    { 0.0, 0.0, 0.0, 0.0},
    0.0
};

#endif