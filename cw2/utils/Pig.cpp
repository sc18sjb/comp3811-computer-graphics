#include <GL/glu.h>
#include <cmath>
#include <stdio.h>
#include "Pig.h"
#include "Materials.h"
#include "Shapes.h"

// Constructor
Pig::Pig(double bl, double ll, float pigSpeed) {
    // Set default values
    bodyLimit = bl;
    legLimit = ll;
    speed = pigSpeed;
    quality = 5;
}

// Calculate the current body angle used in the ossolation.
double Pig::calculateBodyAngle(double time, double limit) {
    limit = (limit == -1) ? bodyLimit : limit;
    return limit * sin(speed * time) + limit;
}

// Calculate the current leg angle used in the ossolation.
double Pig::calculateLegAngle(double time, double limit) {
    limit = (limit == -1) ? legLimit : limit;
    return limit * sin(speed * time) + limit;
}

// Create the pigs head
void Pig::head(double time) {
    double bodyAngle = calculateBodyAngle(time);

    glPushMatrix();
        glRotatef(-bodyLimit, 0.0, 1.0, 0.0);
        glRotatef(bodyAngle, 0.0, 1.0, 0.0);

        // Head.
        glScalef(0.18, 0.18, 0.18);
        GLUsphere(&pinkShinyMaterials, 4 * quality, 4 * quality);

        // right eye.
        glScalef(0.2, 0.2, 0.2);
        glTranslatef(2.0, 2.5, -3.0);
        GLUsphere(&greyShinyMaterials, 4 * quality, 4 * quality);

        // left eye.
        glTranslatef(-4.0, 0.0, 0.0);
        GLUsphere(&greyShinyMaterials, 4 * quality, 4 * quality);

        // nose.
        glScalef(2.0, 2.0, 2.0);
        glTranslatef(1.0, 0.5, 1.0);
        GLUsphere(&pinkShinyMaterials, 4 * quality, 4 * quality);

        // right ear.
        glScalef(0.7, 1.0, 1.4);
        glTranslatef(2.0, -1.5, -1.0);
        GLUsphere(&pinkShinyMaterials, 4 * quality, 4 * quality);

        // left ear.
        glTranslatef(-4.5, 0.0, 0.0);
        GLUsphere(&pinkShinyMaterials, 4 * quality, 4 * quality);
    glPopMatrix();
}

// Create the pigs leg
void Pig::leg(double limit, double time) {
    double legAngle = calculateLegAngle(time, limit);

    glPushMatrix();
        glRotatef(-limit, 1.0, 0.0, 0.0);
        glRotatef(legAngle, 1.0, 0.0, 0.0);
        cylinder(&pinkShinyMaterials, std::max(4, 2 * quality), std::max(4, 2 * quality));
    glPopMatrix();
}

// Create the pigs body and attach the legs
void Pig::body(double time) {
    double bodyAngle = calculateBodyAngle(time);

    glPushMatrix();
        glScalef(0.2, 0.25, 0.15);
        glRotatef(90, 1.0, 0.0, 0.0);
        
        // Rotate ONLY the body.
        glPushMatrix();
            glRotatef(bodyAngle, 0.0, 0.0, 1.0);
            cylinder(&pinkShinyMaterials, 5 * quality, 5 * quality);
        glPopMatrix();

        // Front right leg.
        glRotatef(90, 1.0, 0.0, 0.0);
        glScalef(0.2, 0.2, 1.0);
        glTranslatef(3.0, 3.0, -1.0);   
        leg(-legLimit, time);
        
        // Front left leg.    
        glTranslatef(-6.0, 0.0, 0.0);   
        leg(legLimit, time);

        // Back left leg.   
        glTranslatef(0.0, -6.0, 0.0);   
        leg(legLimit, time);

        // Back right leg.   
        glTranslatef(6.0, 0.0, 0.0);    
        leg(-legLimit, time); 
    glPopMatrix();
}

// Create the back of the pig with a tail :)
void Pig::back(double time) {
    double bodyAngle = calculateBodyAngle(time);

    glPushMatrix();
        // back of pig.
        glScalef(0.2, 0.14, 0.14);
        GLUsphere(&pinkShinyMaterials, 4 * quality, 4 * quality);

        // tail.
        glRotatef(120, 1.0, 0.0, 0.0);
        glRotatef(-bodyAngle, 1.0, 0.0, 0.0);
        glScalef(0.2, 0.2, 0.7);
        glTranslatef(0.0, -1.0, 2.0);
        cylinder(&pinkShinyMaterials, std::max(4, 2 * quality), std::max(4, 2 * quality));
    glPopMatrix();
}