#include <GL/glu.h>
#include <cmath>
#include <cstdio>
#include "Shapes.h"

static const float PI = 3.1415926535;

// Set the material propteries of an object.
void setMaterialProperties(const materialStruct* material) {
    glMaterialfv(GL_FRONT, GL_AMBIENT, material->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, material->specular);
    glMaterialf(GL_FRONT, GL_SHININESS, material->shininess);
}

// Draw a tesselated rectangle plane.
void rectangle(const materialStruct* material, int subDivisions) {
    setMaterialProperties(material);
    float step = 1.0 / subDivisions;
    float x0, x1, y0, y1;

    for (int i = 0; i < subDivisions; i++) {
        for (int j = 0; j < subDivisions; j++) {
            x0 = i * step;
            y0 = j * step;
            x1 = x0 + step;
            y1 = y0 + step;

            // We don't need to calculate z since our rectangle is a 2d plane

            glBegin(GL_POLYGON);
                glTexCoord2f(x0, y0);
                glNormal3f(x0, y0, 0.333);
                glVertex3f(x0, y0, 0.0);

                glTexCoord2f(x1, y0);
                glNormal3f(x1, y0, 0.333);
                glVertex3f(x1, y0, 0.0);

                glTexCoord2f(x1, y1);
                glNormal3f(x1, y1, 0.333);
                glVertex3f(x1, y1, 0.0);

                glTexCoord2f(x0, y1);
                glNormal3f(x0, y1, 0.333);
                glVertex3f(x0, y1, 0.0);
            glEnd();
        }
    }
}

// Draw a tesselated rectangle plane with textures.
void rectangle(const materialStruct* material, Texture* texture, int subDivisions) {
    glEnable(GL_TEXTURE_2D);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->getWidth(), texture->getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, texture->getTexture());
    rectangle(material, subDivisions);
    glDisable(GL_TEXTURE_2D);
}

// Subdivide a triangle defined by vertices A, B, C into 4 smaller triangles from the
// midpoint betweeb AB, BC and CA.
// PLEASE NOTE THIS IMPLEMENTATION IS DIFFERENT TO THAT SHOWN IN THE REPORT TO WORK WITH C++98 - ON THE LAB COMPUTERS
// AS OPPOSED TO C++11 WHICH THE VERSION IN THE REPORT IS 
TriangleVec subDivideTriangle(Vertex A, Vertex B, Vertex C, int n) {
    // Calculate the midpoints for each of the 3 lines creating the triangle,
    // to define the sub triangles.
    float amid[2] = {(A[0] + B[0]) / 2.0, (A[1] + B[1]) / 2.0};
    float bmid[2] = {(B[0] + C[0]) / 2.0, (B[1] + C[1]) / 2.0};
    float cmid[2] = {(C[0] + A[0]) / 2.0, (C[1] + A[1]) / 2.0};
    Vertex Amid (amid, amid + 2);
    Vertex Bmid (bmid, bmid + 2);
    Vertex Cmid (cmid, cmid + 2);

    // Each 'sub' triangle.
    Triangle mid;
    Triangle top;
    Triangle bottomLeft;
    Triangle bottomRight;
    TriangleVec triangle;

    // Push values for mid triangle
    mid.push_back(Amid);
    mid.push_back(Bmid);
    mid.push_back(Cmid);

    // Push values for top triangle
    top.push_back(Cmid);
    top.push_back(Bmid);
    top.push_back(C);

    // Push values for bottom left triangle
    bottomLeft.push_back(A);
    bottomLeft.push_back(Amid);
    bottomLeft.push_back(Cmid);

    // Push values for bottom right triangle
    bottomRight.push_back(Amid);
    bottomRight.push_back(B);
    bottomRight.push_back(Bmid);

    // base case.
    if (n == 1) {
        triangle.push_back(bottomLeft);
        triangle.push_back(bottomRight);
        triangle.push_back(mid);
        triangle.push_back(top);
        return triangle;
    }
    
    // Divide and conqure.
    TriangleVec t1 = subDivideTriangle(mid[0], mid[1], mid[2], n-1);
    TriangleVec t2 = subDivideTriangle(top[0], top[1], top[2], n-1);
    TriangleVec t3 = subDivideTriangle(bottomLeft[0], bottomLeft[1], bottomLeft[2], n-1);
    TriangleVec t4 = subDivideTriangle(bottomRight[0], bottomRight[1], bottomRight[2], n-1);

    // Concatinate all the resulting triangles to create an array of triangle vertex definitions.
    triangle.insert(triangle.end(), t1.begin(), t1.end());
    triangle.insert(triangle.end(), t2.begin(), t2.end());
    triangle.insert(triangle.end(), t3.begin(), t3.end());
    triangle.insert(triangle.end(), t4.begin(), t4.end());

    return triangle;
}

// Draw a tesselated triangle by recursively subdividing it.
// PLEASE NOTE THIS IMPLEMENTATION IS DIFFERENT TO THAT SHOWN IN THE REPORT TO WORK WITH C++98 - ON THE LAB COMPUTERS
// AS OPPOSED TO C++11 WHICH THE VERSION IN THE REPORT IS 
void triangle(const materialStruct* material, int subDivisions) {
    setMaterialProperties(material);

    // Default triangle values for subDivisions = 1.
    float a[2] = {0.0, 0.0};
    float b[2] = {1.0, 0.0};
    float c[2] = {0.5, 1.0};
    Vertex A (a, a + 2);
    Vertex B (b, b + 2);
    Vertex C (c, c + 2);

    Triangle t1;
    TriangleVec triangles;

    // Either explicitally create the triangle or, for subDivisions > 1,
    // divide and conqure the triangle into subTriangles.
    if (subDivisions > 1)
        triangles = subDivideTriangle(A, B, C, subDivisions - 1);
    else {
        t1.push_back(A);
        t1.push_back(B);
        t1.push_back(C);
        triangles.push_back(t1);
    }

    for (unsigned int i = 0; i < triangles.size(); i++) {
        glBegin(GL_POLYGON);
            for (unsigned int j = 0; j < triangles[i].size(); j++) {
                glTexCoord2f(triangles[i][j][0], triangles[i][j][1]);
                glNormal3f(triangles[i][j][0], triangles[i][j][1], 0.333);
                glVertex3f(triangles[i][j][0], triangles[i][j][1], 0.0);
            }
        glEnd();
    }
}

// Draw a tesselated triangle with textures.
void triangle(const materialStruct* material, Texture* texture, int subDivisions) {
    glEnable(GL_TEXTURE_2D);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->getWidth(), texture->getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, texture->getTexture()); 
    triangle(material, subDivisions);
    glDisable(GL_TEXTURE_2D);
}

// Decide whether to texture a triangle face or not based on if the textures array is empty.
void decideTextureTriangle(MaterialVec materials, TextureVec textures, int subDivisions, int n) {
    if (textures.empty())
        triangle(&materials[n], subDivisions);
    else
        triangle(&materials[n], textures[n], subDivisions);
}

// Draw a tesselated tetrahedron out of triangle faces.
void tetrahedron(MaterialVec materials, int subDivisions, TextureVec textures) {
    decideTextureTriangle(materials, textures, subDivisions, 0);

    glPushMatrix();
        glScalef(1.0, 1.11, 1.0);
        glRotatef(71, -1.0, 0.0, 0.0);
        decideTextureTriangle(materials, textures, subDivisions, 1);
    glPopMatrix();

    glPushMatrix();
        glRotatef(90, -1.0, 0.0, 0.0);
        glRotatef(63.5, 0.0, -1.0, 0.0);
        glScalef(1.11, 1.0, 1.0);
        glRotatef(17, -1.0, 0.0, 0.0);
        decideTextureTriangle(materials, textures, subDivisions, 2);
    glPopMatrix();

    glPushMatrix();
        glRotatef(90, -1.0, 0.0, 0.0);
        glTranslatef(1.0, 0.0, 0.0);
        glRotatef(117, 0.0, -1.0, 0.0);
        glScalef(1.11, 1.0, 1.0);
        glRotatef(15.8, 1.0, 0.0, 0.0);
        decideTextureTriangle(materials, textures, subDivisions, 3);
    glPopMatrix();
}

// Decide whether to teture a rectangle face or not based on if the textures array is empty.
void decideTextureRectangle(MaterialVec materials, TextureVec textures, int subDivisions, int n) {
    if (textures.empty())
        rectangle(&materials[n], subDivisions);
    else
        rectangle(&materials[n], textures[n], subDivisions);
}

// Create a cube out of rectangles with each side texured with a different material or texture.
void cube(MaterialVec materials, int subDivisions, TextureVec textures) {
    glPushMatrix();
        decideTextureRectangle(materials, textures, subDivisions, 0);

        glTranslatef(0.0, 0.0, -1.0);
        decideTextureRectangle(materials, textures, subDivisions, 1);        

        glTranslatef(0.0, 0.0, 1.0);
        glRotatef(-90, 1.0, 0.0, 0.0);
        decideTextureRectangle(materials, textures, subDivisions, 2);        

        glTranslatef(0.0, 0.0, 1.0);
        decideTextureRectangle(materials, textures, subDivisions, 3);        

        glRotatef(90, 0.0, 1.0, 0.0);
        decideTextureRectangle(materials, textures, subDivisions, 4);        

        glTranslatef(0.0, 0.0, 1.0);
        decideTextureRectangle(materials, textures, subDivisions, 5);        
    glPopMatrix();
}

// Create a convex cylinder out of faces.
void cylinder(const materialStruct* material, int faces, int heightDivisions) {
    setMaterialProperties(material);
    float x0, x1, y0, y1, z;

    float zMin     = -1;
    float zMax     = 1;
    float deltaZ   = (zMax - zMin) / heightDivisions;

    for (int i = 0; i < faces; i++) {
        for(int j = 0; j < heightDivisions; j++) {
            x0 = cos(2 * i * PI / faces);
            y0 = sin(2 * i * PI / faces);
            x1 = cos(2 * (i + 1) * PI / faces);
            y1 = sin(2 * (i + 1) * PI / faces);
            z = zMin + j * deltaZ;

            glBegin(GL_POLYGON);
                glTexCoord2f(x0, y0);
                glNormal3f(x0, y0, 0.333);
                glVertex3f(x0, y0, z);

                glTexCoord2f(x1, y1);
                glNormal3f(x1, y1, 0.333);
                glVertex3f(x1, y1, z);

                glTexCoord2f(x1, y1);
                glNormal3f(x1, y1, 0.333);
                glVertex3f(x1, y1, z + deltaZ);

                glTexCoord2f(x0, y0);
                glNormal3f(x0, y0, 0.333);
                glVertex3f(x0, y0, z + deltaZ);
            glEnd();
        }
    }
}

// Create a convex cylinder with texturing.
void cylinder(const materialStruct* material, Texture* texture, int faces, int heightDivisions) {
    glEnable(GL_TEXTURE_2D);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->getWidth(), texture->getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, texture->getTexture());     
    cylinder(material, faces, heightDivisions);
    glDisable(GL_TEXTURE_2D);
}

// Create a GLU object sphere.
void GLUsphere(const materialStruct* material, int slices, int stacks){
    setMaterialProperties(material);
    GLUquadric *quad = gluNewQuadric();
    gluQuadricTexture(quad, true);
    gluSphere(quad, 1, slices, stacks);
}

// Create a GLU object sphere with texturing.
void GLUsphere(const materialStruct* material, Texture* texture, int slices, int stacks) {
    glEnable(GL_TEXTURE_2D);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->getWidth(), texture->getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, texture->getTexture()); 
    GLUsphere(material, slices, stacks);
    glDisable(GL_TEXTURE_2D);
}