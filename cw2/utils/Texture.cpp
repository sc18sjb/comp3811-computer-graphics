#include "Texture.h"
#include <vector>
#include <iostream>
#include <cstdlib>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

// Constructor
Texture::Texture(const std::string& fileName) {
    qImage = new QImage(QString(fileName.c_str()));
    
    // Set the width and the height of the texture to the width and the height of the image
    width  = qImage->width();
    height = qImage->height(); 

    unsigned int size = width * height;
    image = new GLubyte[size * 3]; // We * 3 for each rgb value (3 values)

    // int width, height, nrChannels;
    // data = stbi_load(fileName.c_str(), &width, &height, &nrChannels, 0);

    for (unsigned int i = 0; i < size; i++) {
        std::div_t part = std::div((int)i, (int)width);                 // Get the remainder and value from a division
        QRgb colval = qImage->pixel(width - part.rem - 1, part.quot);   // Get the individual pixel in the grid
        image[3 * size - 3 * i - 3] = qRed(colval);                     // Set the R value for the pixel in the image data
        image[3 * size - 3 * i - 2] = qGreen(colval);                   // Set the G value for the pixel in the image data
        image[3 * size - 3 * i - 1] = qBlue(colval);                    // Set the B value for the pixel in the image data
    }
}

// Destructor
Texture::~Texture() {
    // clean up
    delete image;
    delete qImage;
}
