#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QLabel>
#include <QTimer>
#include <QSpinBox>
#include <QCheckBox>
#include <QGridLayout>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QBoxLayout>
#include "SceneWidget.h"

class SceneWindow: public QWidget { 
        public:
                /**
                 * Method SceneWindow
                 * Constructor to add all the components to the window.
                 * 
                 * PARAMS: parent: The QWidget parent object to interhrit from.
                 * */
                SceneWindow(QWidget *parent);

                /**
                 * Method ~SceneWindow
                 * Destructor to clean up all the attributes in the QT window.
                 * */
                ~SceneWindow();

                /**
                 * Method resetInterface
                 *  Resets all the interface elements.
                 * */
                void resetInterface();
                
                // Setup components.
                QMenuBar *menuBar;                      // Menu bar.
                QMenu *fileMenu;                        // File menu.
                QAction *actionQuit;                    // Quit action.
                QGridLayout *windowLayout;              // Window layout.

                SceneWidget *sceneWidget;               // The actual 'scene'.

                // Interactive world components.
                QGroupBox *worldControls;               // Seperate the world controls.
                QVBoxLayout *worldControlsLayout;       // Group the world controls.
                QLabel *zoomSliderLabel;                
                QSlider *zoomSlider;
                QLabel *rotateSliderLabel;
                QSlider *rotateSlider;
                QLabel *qualitySliderLabel;
                QSlider *qualitySlider;
                QLabel *smoothLightingCheckboxLabel;
                QCheckBox *smoothLightingCheckbox;
                QTimer *animationTimer;
                
                // Interactive pig components.
                QGroupBox *pigControls;                 // Seperate the pig controls
                QVBoxLayout *pigControlsLayout;         // Group the pig controls
                QLabel *pigRadiusLabel;
                QSpinBox *pigRadius;
                QLabel *pigSpeedLabel;
                QSpinBox *pigSpeed;
}; 
	
#endif
