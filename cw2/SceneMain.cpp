#include <QApplication>
#include <QVBoxLayout>
#include "SceneWindow.h"

int main(int argc, char *argv[]) {
	// Create the application.
	QApplication app(argc, argv);

	// Create a master window widget.
    SceneWindow *window = new SceneWindow(NULL);

	// Tesize the window.
	window->resize(612, 612);

	// Set the title of the window to this amazing pun :)
	window->setWindowTitle("Sam's Barns by Sam Barnes");

	// Show the window.
	window->show();

	// Start the app.
	app.exec();

	// Clean up.
	delete window;

	// Return to caller.
	return 0; 
}
