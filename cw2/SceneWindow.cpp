#include "SceneWindow.h"

// Constructor
SceneWindow::SceneWindow(QWidget *parent) : QWidget(parent) {
	menuBar = new QMenuBar(this);
	actionQuit = new QAction("&Quit", this);
	windowLayout = new QGridLayout(this);

	sceneWidget = new SceneWidget(this);
	sceneWidget->setMinimumSize(600, 500);

	/**
	 * All controls related to the scene world.
	 * */
	worldControls 		= new QGroupBox("World Controls", this);
	worldControlsLayout = new QVBoxLayout;

	zoomSliderLabel = new QLabel("World Zoom:");
	zoomSlider 		= new QSlider(Qt::Horizontal);
	zoomSlider->setRange(1, 10);
	zoomSlider->setValue(sceneWidget->getZoom());

	rotateSliderLabel 	= new QLabel("World Rotation:");
	rotateSlider 		= new QSlider(Qt::Horizontal);
	rotateSlider->setRange(1, 360);
	rotateSlider->setValue(sceneWidget->getRotate());

	qualitySliderLabel 	= new QLabel("World Quality");
	qualitySlider 		= new QSlider(Qt::Horizontal);
	qualitySlider->setRange(1, 6); // We lowered the world quality from 10 => 6 because tetrahedrons are laggy apparently
	qualitySlider->setValue(sceneWidget->getQuality());

	smoothLightingCheckboxLabel = new QLabel("Smooth Lighting");
	smoothLightingCheckbox 		= new QCheckBox;
	smoothLightingCheckbox->setCheckState(Qt::Checked);

	animationTimer = new QTimer;
	animationTimer->start(10);

	/**
	 * All controls relating to the animated pig.
	 * */
	pigControls 		= new QGroupBox("Pig Controls", this);
	pigControlsLayout 	= new QVBoxLayout();

	pigRadiusLabel 	= new QLabel("Pig Walking Radius");
	pigRadius 		= new QSpinBox;
	pigRadius->setRange(1, 15);
	pigRadius->setSingleStep(1);
	pigRadius->setValue(sceneWidget->getPigRadius() * 10);

	pigSpeedLabel 	= new QLabel("Pig Walking Speed");
	pigSpeed 		= new QSpinBox;
	pigSpeed->setRange(1, 5);
	pigSpeed->setSingleStep(1);
	pigSpeed->setValue(sceneWidget->getPigSpeed() * 10);

	/**
	 * Connect all the interactive components to their appropriate method.
	 * */
	connect(zoomSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updateZoomRange(int)));
	connect(rotateSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updateRotateRange(int)));
	connect(qualitySlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updateQuality(int)));
	connect(animationTimer, SIGNAL(timeout()), sceneWidget, SLOT(updateAngle()));
	connect(pigRadius, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updatePigRadius(int)));
	connect(pigSpeed, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updatePigSpeed(int)));
	connect(smoothLightingCheckbox, SIGNAL(stateChanged(int)), sceneWidget, SLOT(setSmoothLighting(int)));

	/**
	 * Add everything to the scene
	 * */
	windowLayout->addWidget(sceneWidget, 0, 0, 2, 0 );

	// World controls
	worldControlsLayout->addWidget(zoomSliderLabel);
	worldControlsLayout->addWidget(zoomSlider);
	worldControlsLayout->addWidget(rotateSliderLabel);
	worldControlsLayout->addWidget(rotateSlider);
	worldControlsLayout->addWidget(qualitySliderLabel);
	worldControlsLayout->addWidget(qualitySlider);
	worldControlsLayout->addWidget(smoothLightingCheckboxLabel);
	worldControlsLayout->addWidget(smoothLightingCheckbox);
	worldControls->setLayout(worldControlsLayout);
	windowLayout->addWidget(worldControls);

	// Pig controls
	pigControlsLayout->addWidget(pigRadiusLabel);
	pigControlsLayout->addWidget(pigRadius);
	pigControlsLayout->addWidget(pigSpeedLabel);
	pigControlsLayout->addWidget(pigSpeed);
	pigControls->setLayout(pigControlsLayout);
	windowLayout->addWidget(pigControls, 2, 1);
}

// Destructor.
SceneWindow::~SceneWindow() {
	// clean up.
	delete pigSpeed;
	delete pigSpeedLabel;
	delete pigRadius;
	delete pigRadiusLabel;
	delete pigControlsLayout;
	delete pigControls;
	delete animationTimer;
	delete smoothLightingCheckbox;
	delete smoothLightingCheckboxLabel;
	delete qualitySlider;
	delete qualitySliderLabel;
	delete rotateSlider;
	delete rotateSliderLabel;
	delete zoomSlider;
	delete zoomSliderLabel;
	delete worldControlsLayout;
	delete worldControls;
	delete sceneWidget;
	delete windowLayout;
	delete actionQuit;
	delete fileMenu;
	delete menuBar;
}

// Resets all the interface elements.
void SceneWindow::resetInterface() {
	// Update events
	sceneWidget->update();
	update();
}