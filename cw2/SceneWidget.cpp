#include <GL/glu.h>
#include <QGLWidget>
#include <cstdio>
#include <stdio.h>
#include <cmath>
#include "SceneWidget.h"
#include "utils/Materials.h"


// Constructor
SceneWidget::SceneWidget(QWidget *parent) : QGLWidget(parent){
    // Set value defaults
    zoom            = 1.0;
    rotate          = 1.0;
    angle           = 0.0;
    pigRadius       = 0.1;
    pigSpeed        = 0.1;
    quality         = 4;
    smoothLighting  = 1;

    // Add the textures to the texture map
    textureMap["marc"]          = new Texture("textures/Marc_Dekamps.jpg");
    textureMap["markus"]        = new Texture("textures/markus.jpg");
    textureMap["earth"]         = new Texture("textures/Mercator-projection.jpg");
    textureMap["grass"]         = new Texture("textures/grass.jpg");
    textureMap["bush"]          = new Texture("textures/bush.jpg");
    textureMap["road"]          = new Texture("textures/road.jpg");
    textureMap["roofTile"]      = new Texture("textures/roofTile.jpg");
    textureMap["metalDoor"]     = new Texture("textures/metalDoor.jpg");
    textureMap["metalPanal"]    = new Texture("textures/metalRoof.jpg");
    textureMap["barnWall"]      = new Texture("textures/barnWall.jpg");
    textureMap["barnDoor"]      = new Texture("textures/barnWallDoor.jpg");
    textureMap["barnSign"]      = new Texture("textures/barnSign.jpg");

    pig = new Pig(20.0, 50.0, pigSpeed);
    pig->setQuality(quality);
}

// Destructor.
SceneWidget::~SceneWidget() {
    // Clean up
    delete pig;

    // Clear all the pointers in the textureMap
    std::map<std::string, Texture*>::iterator it;
    for (it = textureMap.begin(); it != textureMap.end(); it++) {
        if (it->second != NULL)
            delete it->second;
    }
    textureMap.clear();
}

// Update the world zoom range, set the zoom value to the value from the zoom slider.
void SceneWidget::updateZoomRange(int value) {
    zoom = value;
    repaint();
}

// Update the world rotation range, set the rotation value to the value from the rotation slider.
void SceneWidget::updateRotateRange(int value) {
    rotate = value;
    repaint();
}

// Update the world quality, that is the number of planes in a tesselated object.
// Set the quality value to the value from the quality slider.
void SceneWidget::updateQuality(int value) {
    quality = value;
    pig->setQuality(quality);
    repaint();   
}

// Enable / disable smooth lighting on the world: state is the bool here.
void SceneWidget::setSmoothLighting(int state) {
    smoothLighting = state;
    repaint();
}

// Update the angle for any animated component in the world,
// this is an 'infinately' increasing value since OpenGL automatically wraps
// back around when an angle > 360.
void SceneWidget::updateAngle() {
    angle += 1;
    repaint();
}

// Update the walking radius of the heirarchy pig. Set the radius value to the value from the spinbox.
void SceneWidget::updatePigRadius(int value) {
    pigRadius = (value / 10.0);
    repaint();
}

// Update the walking speed of the heirarchy pig. Set the speed value to the value from the spinbox.
void SceneWidget::updatePigSpeed(int value) {
    pigSpeed = (value / 10.0);
    pig->setSpeed(pigSpeed);
    repaint();
}

// Called when OpenGL context is setup.
void SceneWidget::initializeGL() {
    // Set the widget background color.
    glClearColor(0.529, 0.807, 1.0, 0.921);

    // Enable lighting.
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    // Set the lighting position.
	GLfloat light_pos[] = {10.0, -5.0, 3.0, 1.0};
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

    // Configure texturing.
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Needed for markus's face :)
}

// Called every time the widget is resized.
void SceneWidget::resizeGL(int w, int h) {
    // Resize the display and everything within it
    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    // Set the projection matrix to this matrix, the the near and far clipping plane values.
    glFrustum(-1.0, 1.0, -1.0, 1.0, 1.0, 1000.0);
}

// Create a house object out of cubes.
void SceneWidget::house(float w, float h, float d, MaterialVec wallMaterials, MaterialVec roofMaterials, TextureVec wallTextures, TextureVec roofTextures) {
    glPushMatrix();
        glScalef(0.5 * w, 0.5 * h, 0.5 * d);
        cube(wallMaterials, 10 * quality, wallTextures);

        glScalef(0.71, 0.98, 1.0);
        glTranslatef(0.7, 0.01, -0.3);
        glRotatef(45, 0.0, 1.0, 0.0);
        cube(roofMaterials, 10 * quality, roofTextures);
    glPopMatrix();
}

// Create the scene ground object out of rectangles.
void SceneWidget::ground() {
    // Ground
    glPushMatrix();
        glScalef(2.0, 2.0, 2.0);
        glTranslatef(-0.5, -0.5, 0.5);
        rectangle(&greenShinyMaterials, textureMap["grass"], 20 * quality);

        // Draw border around ground to show depth
        glPushMatrix();
            glRotatef(90, 1.0, 0.0, 0.0);
            glScalef(1.0, 0.2, 1.0);
            rectangle(&brownShinyMaterials, 2 * quality);

            glTranslatef(0.0, 0.0, -1.0);
            rectangle(&brownShinyMaterials, 2 * quality);

            glTranslatef(0.0, 0.0, 1.0);
            glRotatef(90, 0.0, 1.0, 0.0);
            rectangle(&brownShinyMaterials, 2 * quality);

            glTranslatef(0.0, 0.0, 1.0);
            rectangle(&brownShinyMaterials, 2 * quality);
        glPopMatrix();

        // Dirt patch
        glScalef(0.4, 0.35, 0.3);
        glTranslatef(1.3, 0.1, -0.001);
        rectangle(&brownShinyMaterials, textureMap["grass"], 10 * quality);
    glPopMatrix();

    // Road
    glPushMatrix();
        glScalef(2.0, -0.3, 1.0);
        glTranslatef(-0.5, -0.5, 0.999);
        rectangle(&greyShinyMaterials, textureMap["road"], 10 * quality);
    glPopMatrix();

    // Road markers
    glPushMatrix();
        glScalef(-0.3, -0.025, 1.0);
        glTranslatef(1.5, -0.5, 0.99);
        rectangle(&whiteShinyMaterials, 2 * quality);

        glTranslatef(-2.0, 0.0, 0.0);
        rectangle(&whiteShinyMaterials, 2 * quality);

        glTranslatef(-2.0, 0.0, 0.0);
        rectangle(&whiteShinyMaterials, 2 * quality);
    glPopMatrix();
}

// Create a bush object out of tetrahedrons.
void SceneWidget::bush() {
    // A bush is made up of randomly translated and rotated tetragedrons (3 of them)
    // (there is no real 'formula' here, there are just placed until it looks natural).
    glPushMatrix();
        glScalef(0.3, 0.3, 0.3);
        tetrahedron(MaterialVec (6, greenShinyMaterials), std::max(2, 1 * quality), TextureVec(6, textureMap["bush"]));

        glTranslatef(0.5, 0.0, 0.0);
        glRotatef(60, 0.0, 0.0, 1.0);
        tetrahedron(MaterialVec (6, greenShinyMaterials), std::max(2, 1 * quality), TextureVec(6, textureMap["bush"]));

        glTranslatef(0.2, 0.0, 0.0);
        glRotatef(20, 0.0, 1.0, 1.0);
        tetrahedron(MaterialVec (6, greenShinyMaterials), std::max(2, 1 * quality), TextureVec(6, textureMap["bush"]));
    glPopMatrix();

}

// Create a silo object to attach to the house to create the barn.
void SceneWidget::silo() {
    glPushMatrix();
        glScalef(0.2, 0.2, 0.5);
        cylinder(&redMaterials, textureMap["barnWall"], std::max(10, 2 * quality), std::max(10, 2 * quality));

        glScalef(0.95, 0.95, 0.75);
        glTranslatef(0.0, 0.0, -1);
        GLUsphere(&greyShinyMaterials, textureMap["metalPanal"], 20 * quality, 20 * quality);
    glPopMatrix();
}

// Create a fence object out of n number of partitians defining the length of the fence.
void SceneWidget::fence(int partitians) {
    // Fence poles.
    glPushMatrix();
        glRotatef(90, 1.0, 0.0, 0.0);
        glScalef(0.03, 0.3, 0.03);

        // We + 1 for since there is 2 poles for each plank.
        for (int i = 0; i < partitians + 1; ++i) {
            cube(MaterialVec (6, brownShinyMaterials), 2 * quality);    
            glTranslatef(10.0, 0.0, 0.0);            
        }
    glPopMatrix();

    // Fence planks.
    glPushMatrix();
        glRotatef(90, 0.0, 0.0, -1.0);    
        glScalef(0.01, 0.33, 0.03);
        glTranslatef(0.0, 0.0, 1.0);

        for (int i = 0; i < partitians; ++i) {
            cube(MaterialVec (6, brownShinyMaterials), 2 * quality);

            glTranslatef(0.0, 0.0, 2.0);
            cube(MaterialVec (6, brownShinyMaterials), 2 * quality);   
            
            glTranslatef(0.0, 0.0, 2.0);
            cube(MaterialVec (6, brownShinyMaterials), 2 * quality);           

            glTranslatef(0.0, 0.0, 2.0);
            cube(MaterialVec (6, brownShinyMaterials), 2 * quality);   

            glTranslatef(0.0, 1.0, -6.0);
            if (i == 0)
                glScalef(1.0, 0.9, 1.0);
        }
    glPopMatrix();
}

// Create a road billboard style sign object held up by poles.
void SceneWidget::sign(Texture* texture) {
    // Sign columns.
    glPushMatrix();
        glScalef(0.025, 0.025, 0.3);
        cylinder(&greyShinyMaterials, std::max(4, 2 * quality), std::max(4, 2 * quality));
        glTranslatef(12.0, 0.0, 0.0);
        cylinder(&greyShinyMaterials, std::max(4, 2 * quality), std::max(4, 2 * quality)); 
    glPopMatrix();

    // Sign face.
    glPushMatrix();
        glRotatef(90, -1.0, 0.0, 0.0);
        glScalef(0.3, 0.5, 0.5);
        rectangle(&greyShinyMaterials, texture, 20 * quality);
    glPopMatrix();
}

// Create an animated 'pub' style sign object to be attached to the front of the barn.
void SceneWidget::wavingSign(Texture* texture, double angle) {
    double waveAngle = 45 * sin(0.1 * angle) + 45;

    // Sign handle (that holds the sign).
    glPushMatrix();
        glScalef(0.15, 0.02, 0.02);
        glRotatef(90, 0.0, 1.0, 0.0);
        cylinder(&brownShinyMaterials, std::max(4, 2 * quality), std::max(4, 2 * quality));
    glPopMatrix();

    // Sign face with texture.
    glPushMatrix();
        glRotatef(90, 1.0, 0.0, 0.0);
        glScalef(0.2, 0.2, 0.2);
        glTranslatef(-0.25, 0.0, 0.0);
        glRotatef(45, 1.0, 0.0, 0.0);
        glRotatef(-waveAngle, 1.0, 0.0, 0.0);
        rectangle(&greyShinyMaterials, texture, 4 * quality);
    glPopMatrix();
}

// Create a picture frame object containing an image (texture) to be shown on the side of the shed.
void SceneWidget::pictureFrame(Texture* texture) {
    // Left and right picture borders.
    glPushMatrix();
        glScalef(0.02, 0.25, 0.02);
        cube(MaterialVec (6, brownShinyMaterials), 2 * quality);

        glTranslatef(0.0, 0.0, -11.5);
        cube(MaterialVec (6, brownShinyMaterials), 2 * quality);        
    glPopMatrix(); 
    
    // Top and Bottom picture borders.
    glPushMatrix();
        glRotatef(90, -1.0, 0.0, 0.0);
        glScalef(0.02, 0.25, 0.02);            
        cube(MaterialVec (6, brownShinyMaterials), 2 * quality);  
        
        glTranslatef(0.0, 0.0, 13.5);
        cube(MaterialVec (6, brownShinyMaterials), 2 * quality);               
    glPopMatrix();

    // Picture face itself with texture.
    glPushMatrix();
        glRotatef(90, 0.0, 1.0, 0.0);
        glRotatef(90, 0.0, 0.0, -1.0);
        glScalef(0.25, 0.25, 0.25);
        glTranslatef(-1.0, 0.0, 0.0);
        rectangle(&greyShinyMaterials, texture, 4 * quality);        
    glPopMatrix();
}

// Create a heirarchicle pig object to walk in a circle through the scene.
void SceneWidget::piggy() {
    glPushMatrix();
        // Make the whole pig smaller.
        glScalef(0.5, 0.5, 0.5);

        // Body and Legs.
        pig->body(angle);

        // Head.
        glPushMatrix();
            glTranslatef(0.0, 0.25, -0.05);
            pig->head(angle);
        glPopMatrix();

        // Back.
        glPushMatrix();
            glTranslatef(0.0, -0.22, 0.0);
            pig->back(angle);
        glPopMatrix();
    glPopMatrix();
}

// Create the final scene, put everything togother by using the above methods to place different objects
// around the scene, transitioning, scaling and rotating objects where needed to place them in
// the correct place.
void SceneWidget::scene() {
    // Texture vectors for buildings.
    MaterialVec wallMaterials;
    MaterialVec roofMaterials;
    TextureVec wallTextures;
    TextureVec roofTextures;
    
    // Draw the ground, and any elements on the ground.
    ground();

    // Draw a barn
    glPushMatrix();
        // Insert the barn materials to allow for individual texturing on each face.
        wallMaterials.insert(wallMaterials.begin(), 5, redMaterials);
        wallMaterials.insert(wallMaterials.begin() + 3, greyMaterials);

        roofMaterials.insert(roofMaterials.begin(), 4, greyShinyMaterials);
        roofMaterials.insert(roofMaterials.begin() + 2, 2, redMaterials);
 
        wallTextures.insert(wallTextures.begin(), 5, textureMap["barnWall"]);
        wallTextures.insert(wallTextures.begin() + 3, textureMap["barnDoor"]);

        roofTextures.insert(roofTextures.begin(), 4, textureMap["metalPanal"]);
        roofTextures.insert(roofTextures.begin() + 2, 2, textureMap["barnWall"]);

        glTranslatef(-0.8, -0.8, 1.0);
        house(1.0, 1.0, 1.0, wallMaterials, roofMaterials, wallTextures, roofTextures);
    glPopMatrix();

    // Draw a waving sign attached to the barn.
    glPushMatrix();
        glRotatef(90, 0.0, 0.0, 1.0);
        glTranslatef(-0.2, 0.4, 0.5);
        wavingSign(textureMap["barnSign"], angle);
    glPopMatrix();

    // Draw a silo attached to the barn.
    glPushMatrix();
        glTranslatef(-0.2, -0.7, 0.5);
        silo();
    glPopMatrix();

    // Draw a shed.
    glPushMatrix();
        // Clear the existing barn materials and textures (since we do not want to apply these onto the shed).
        wallMaterials.clear();
        roofMaterials.clear();
        wallTextures.clear();
        roofTextures.clear();

        // Insert the shed textures to allow for individual texturing on each face.
        wallMaterials.insert(wallMaterials.begin(), 6, brickShinyMaterials);
        
        roofMaterials.insert(roofMaterials.begin(), 4, greyShinyMaterials);
        roofMaterials.insert(roofMaterials.begin() + 2, 2, brickShinyMaterials);

        wallTextures.insert(wallTextures.begin(), 5, textureMap["metalPanal"]);
        wallTextures.insert(wallTextures.begin() + 2, textureMap["metalDoor"]);

        roofTextures.insert(roofTextures.begin(), 4, textureMap["roofTile"]);
        roofTextures.insert(roofTextures.begin() + 2, 2, textureMap["metalPanal"]);

        glTranslatef(0.4, 0.5, 1.0);
        house(1.0, 0.8, 0.8, wallMaterials, roofMaterials, wallTextures, roofTextures);
    glPopMatrix();

    // Draw a pictureframe on the shed.
    glPushMatrix();
        glRotatef(180, 0.0, 0.0, 1.0);
        glTranslatef(-0.39, -0.82, 0.9);
        pictureFrame(textureMap["earth"]);
    glPopMatrix();

    // Draw a sign.
    glPushMatrix();
        glTranslatef(0.75, -0.4, 0.7);
        glRotatef(50, 0.0, 0.0, 1.0);
        sign(textureMap["marc"]);
    glPopMatrix();

    // Draw another sign.
    glPushMatrix();
        glTranslatef(-0.5, 0.3, 0.7);
        glRotatef(125, 0.0, 0.0, 1.0);
        sign(textureMap["markus"]);
    glPopMatrix();

    // Draw a hieryarchy piggy wiggy.
    glPushMatrix();
        // Move the pig
        glTranslatef(0.4, -0.6, 0.85);

        // Rotate the pig in a cirle
        glRotatef(angle * (pigSpeed * 10), 0.0, 0.0, 1.0);
        
        // Set the circle radius
        glTranslatef(pigRadius, -pigRadius, 0.0);

        // Rotate the pig in the angle of the circle
        glRotatef(-45, 0.0, 0.0, 1.0);
        piggy();
    glPopMatrix();

    // Draw a penn around the dirt path, fencing in the pig.
    glPushMatrix();
        glScalef(0.85, 1.0, 1.0);
        glTranslatef(0.05, -0.9, 0.7);
        fence(3);

        glRotatef(180, 0.0, 0.0, 1.0);
        glTranslatef(-0.95, -0.65, 0.0);
        fence(3);        

        glRotatef(90, 0.0, 0.0, 1.0);
        glScalef(0.7, 1.0, 1.0);
        glTranslatef(0.0, -0.95, 0.0);
        fence(3);                

        glRotatef(180, 0.0, 0.0, 1.0);
        glTranslatef(-0.95, -0.95, 0.0);
        fence(3);                        
    glPopMatrix();

    // Draw a fence along the road.
    glPushMatrix();
        glScalef(1.05, 1.0, 1.0);
        glRotatef(180, 0.0, 0.0, 1.0);
        glTranslatef(-0.9, -0.25, 0.7);
        fence(6);
    glPopMatrix();

    // Place some bushes around the map.
    glPushMatrix();
        glTranslatef(-0.4, -0.6, 1.0);
        bush();

        glTranslatef(-0.5, -0.3, 0.0);
        bush();

        glTranslatef(1.62, 1.57, 0.0);
        bush();

        glTranslatef(-1.3, 0.0, 0.0);
        bush();

        // Line of bushed by the sign.
        glTranslatef(0.0, -0.2, 0.0);
        bush();
        glTranslatef(0.05, -0.2, 0.0);
        bush();
        glTranslatef(-0.15, -0.1, 0.0);
        bush();
    glPopMatrix();
}

// Called every time the widget needs to be painted.
void SceneWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_NORMALIZE);

    // If smooth lighting is disabled, use flat lighting instead.
    if (!smoothLighting)
        glShadeModel(GL_FLAT);
    else
        glShadeModel(GL_SMOOTH);

    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);

    glLoadIdentity();
    gluLookAt(
        1.0, 1.0, 1.0,
        -0.2, -1.0, 0.0, // We point the 'camera' down to -0.7 since the world is below the origin.
        0.0, 1.0, 0.0
    );

    glScalef(zoom, zoom, zoom);

    // Rotate the world 'left' to see the ground border.
    glRotatef(20, 0.0, 1.0, 0.0);
    
    // Rotate the world 'up' to get a better view.
    glRotatef(-10, 1.0, 0.0, 0.0);
    
    // Apply UI slider rotation.
    glRotatef(rotate, 0.0, 1.0, 0.0);

    // Rotate everything so it is flat against the 'camera'.
    glRotatef(90, 1.0, 0.0, 0.0);
    scene();

    glFlush();
}