#include <iostream>
#include <GL/glu.h>
#include <QGLWidget>
#include "SolidCubeWidget.h"


// Setting up material properties
typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;


static materialStruct brassMaterials = {
  { 0.33, 0.22, 0.03, 1.0},
  { 0.78, 0.57, 0.11, 1.0},
  { 0.99, 0.91, 0.81, 1.0},
  27.8 
};

static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0 
};



// constructor
SolidCubeWidget::SolidCubeWidget(QWidget *parent):
  QGLWidget(parent),
  _angle(0.0),
  _image("earth.ppm")
	{ // constructor

	} // constructor


// called when OpenGL context is set up
void SolidCubeWidget::initializeGL()
	{ // initializeGL()
	// set the widget background colour
	glClearColor(0.3, 0.3, 0.3, 0.0);
 
	} // initializeGL()


// called every time the widget is resized
void SolidCubeWidget::resizeGL(int w, int h)
	{ // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);
	GLfloat light_pos[] = {0., 0., 1., 0.};

	glEnable(GL_LIGHTING); // enable lighting in general
        glEnable(GL_LIGHT0);   // each light source must also be enabled
       	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D); // enable texturing of the object

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
	glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,180.);

  // add the texture
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image.Width(), _image.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image.imageField()); 

  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-4.0, 4.0, -4.0, 4.0, -4.0, 4.0);

	} // resizeGL()


void SolidCubeWidget::cube(){


  // Here are the normals, correctly calculated for the cube faces below                                                                                                            
  GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0, 1, 0}, {0, -1, 0} };


  // // create one face with brassMaterials
  // materialStruct* p_front = &whiteShinyMaterials;
	
  // glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  // glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  // glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  // glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

 
  // glNormal3fv(normals[0]);
  // glBegin(GL_POLYGON);
  //   glVertex3f( 1.0, -1.0,  1.0);
  //   glVertex3f( 1.0, -1.0, -1.0);
  //   glVertex3f( 0.0,  1.0, -1.0);
  //   glVertex3f( 1.0,  1.0,  1.0);
  // glEnd();


  // create one face with brassMaterials                                                                                                                      


  // glNormal3fv(normals[3]); 
  // glBegin(GL_POLYGON);
  //   glVertex3f(-1.0, -1.0, -1.0);
  //   glVertex3f( 1.0, -1.0, -1.0);
  //   glVertex3f( 1.0,  1.0, -1.0);
  //   glVertex3f(-1.0,  1.0, -1.0);
  // glEnd();

  /*  p_front = &brassMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
  */
  glNormal3fv(normals[2]); 
  glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(-1.0, -1.0, 1.0);
    glTexCoord2f(1.0, 0.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glTexCoord2f(1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  // and the others white
  // p_front = &whiteShinyMaterials;
	
  // glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  // glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  // glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  // glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
  
  glColor3f(0.0,0.0,1.0);
  glBegin(GL_POLYGON);
    glVertex3f( 1.0, -1.0,  1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f( 1.0,  1.0,  1.0);
    glEnd();

  glColor3f(1.0,1.0,1.0);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, 1.0, -1.0);
    glVertex3f( -1.0, 1.0, 1.0);
    glVertex3f(-1.0,  -1.0, 1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
  glEnd();

  glColor3f(1.0,0.0,0.0);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();

  glColor3f(0.0,1.0,0.0);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, 1.0, 1.0);
    glVertex3f( -1.0, 1.0, -1.0);
    glVertex3f(1.0,  1.0, -1.0);
    glVertex3f( 1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f(1.0,  -1.0, -1.0);
    glVertex3f( 1.0,  -1.0, 1.0);
  glEnd();

}

void SolidCubeWidget::updateAngle(){
  _angle += 1.0;
  this->repaint();
}	

// called every time the widget needs painting
void SolidCubeWidget::paintGL()
	{ // paintGL()
	// clear the widget
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// You must set the matrix mode to model view directly before enabling the depth test
      	glMatrixMode(GL_MODELVIEW);

	// glRotatef(_angle, 0.,1., 0.);

	glScalef(2.0,2.0,2.0);
            	this->cube();
	glLoadIdentity();
       	gluLookAt(1.,1.,1., 0.0,0.0,0.0, 0.0,1.0,0.0);

      	glRasterPos2i(-1.,-1.);
       	// glDrawPixels(_image.Width(),_image.Height(),GL_RGB, GL_UNSIGNED_BYTE,_image.imageField());

	// flush to screen
	glFlush();	

	} // paintGL()
