#include <GL/glu.h>
#include <QGLWidget>
#include "PlaneWidget.h"


// Setting up material properties
typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;


static materialStruct brassMaterials = {
  { 0.33, 0.22, 0.03, 1.0},
  { 0.78, 0.57, 0.11, 1.0},
  { 0.99, 0.91, 0.81, 1.0},
  27.8 
};

static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0 
};



static materialStruct redShinyMaterials = {
  { 0.5, 0.0, 0.0, 1.0},
  { 0.5, 0.0, 0.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0
};

static materialStruct greenShinyMaterials = {
  { 0.0, 0.0, 0.5, 1.0},
  { 0.0, 0.8, 0.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0 
};



// constructor
PlaneWidget::PlaneWidget(QWidget *parent):
  QGLWidget(parent),
  _angle(0.0)
	{ // constructor
       

	} // constructor


// called when OpenGL context is set up
void PlaneWidget::initializeGL()
	{ // initializeGL()
	// set the widget background colour
	glClearColor(0.3, 0.3, 0.3, 0.0);
 
	} // initializeGL()


// called every time the widget is resized
void PlaneWidget::resizeGL(int w, int h)
	{ // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);

	glEnable(GL_LIGHTING); // enable lighting in general
        glEnable(GL_LIGHT0);   // each light source must also be enabled

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GLfloat light_pos[] = {2., 0., 0., 0.};
	GLfloat spot_direction [] = {-1, 0, 0, 0};
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
	//       	glLightf (GL_LIGHT0, GL_SPOT_CUTOFF, 5.);
       	//              glLightfv (GL_LIGHT0, GL_SPOT_DIRECTION,spot_direction);

        
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-4.0, 4.0, -4.0, 4.0, -4.0, 4.0);

	} // resizeGL()


void PlaneWidget::plane(){

    // create one face with brassMaterials
  materialStruct* p_front = &redShinyMaterials;
	
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);


  GLfloat normals[][3] = { {0., 0. ,1.} };
  
  glNormal3fv(normals[0]);
  glBegin(GL_POLYGON);
    glVertex3f( -2.0, -1.0,  0.0);
    glVertex3f(  2.0, -1.0,  0.0);
    glVertex3f(  2.0,  1.0,  0.0);
    glVertex3f( -2.0,  1.0,  0.0);
  glEnd();
  
  
}

void PlaneWidget::updateAngle(){
  _angle += 1.0;
  this->repaint();
}	

// called every time the widget needs painting
void PlaneWidget::paintGL()
	{ // paintGL()
	// clear the widget
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// You must set the matrix mode to model view directly before enabling the depth test
      	glMatrixMode(GL_MODELVIEW);
       	glEnable(GL_DEPTH_TEST);
	glLoadIdentity();
       	gluLookAt(0.9,0.0,1., 0.0,0.0,0.0, 0.0,1.0,0.0);

	glRotatef(_angle, 0.,1., 0.);

	this->plane();
	glLoadIdentity();
	
	
	// flush to screen
	glFlush();	

	} // paintGL()
