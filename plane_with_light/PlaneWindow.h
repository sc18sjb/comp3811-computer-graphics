#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QTimer>
#include <QBoxLayout>
#include "PlaneWidget.h"

class PlaneWindow: public QWidget
	{ 
	public:
       
	
	// constructor / destructor
	PlaneWindow(QWidget *parent);
	~PlaneWindow();

	// visual hierarchy
	// menu bar
	QMenuBar *menuBar;
		// file menu
		QMenu *fileMenu;
			// quit action
			QAction *actionQuit;

	// window layout
	QBoxLayout *windowLayout;

	// beneath that, the main widget
	PlaneWidget *cubeWidget;
	// and a slider for the number of vertices
	QSlider *nVerticesSlider;

	// a timer
	QTimer *ptimer;

	// resets all the interface elements
	void ResetInterface();
	}; 
	
#endif
