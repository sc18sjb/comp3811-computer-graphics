#include <GL/glu.h>
#include <QGLWidget>
#include <cstdio>
#include "SolidCubeWidget.h"

float _time = 0;




// constructor
SolidCubeWidget::SolidCubeWidget(QWidget *parent): 
  QGLWidget(parent),
  _image("earth.ppm")
	{ // constructor
       
	} // constructor


void SolidCubeWidget::UpdateAngle() {
  _time += 1.0;
  this->repaint();
}

// called when OpenGL context is set up
void SolidCubeWidget::initializeGL()
	{ // initializeGL()
	// set the widget background colour
	glClearColor(0.3, 0.3, 0.3, 0.0);
	
	glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-6.0, 6.0, -6.0, 6.0, -6.0, 6.0);


  // You must set the matrix mode to model view directly before enabling the depth test                                                                                        
  glMatrixMode(GL_MODELVIEW);
  glEnable(GL_DEPTH_TEST); // comment out depth test to observe the result                                                                                              
	
  
  // glEnable(GL_LIGHTING);
  // glEnable(GL_LIGHT0); 
  // glEnable(GL_TEXTURE_2D);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image.Width(), _image.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image.imageField()); 
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  
  } // initializeGL()

// called every time the widget is resized
void SolidCubeWidget::resizeGL(int w, int h)
	{ // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);

	} // resizeGL()


void SolidCubeWidget::cube(){
  // GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0, 1, 0}, {0, -1, 0} };
  // glNormal3fv(normals[2]); 
  // glBegin(GL_POLYGON);
  //   glTexCoord2f(0.0, 0.0);
  //   glVertex3f(-1.0, -1.0, 1.0);
  //   glTexCoord2f(1.0, 0.0);
  //   glVertex3f( 1.0, -1.0, 1.0);
  //   glTexCoord2f(1.0, 1.0);
  //   glVertex3f( 1.0,  1.0, 1.0);
  //   glTexCoord2f(0.0, 1.0);
  //   glVertex3f(-1.0,  1.0, 1.0);
  // glEnd();

  glColor3f(0.0,0.0,1.0);
  glBegin(GL_POLYGON);
    glVertex3f( 1.0, -1.0,  1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f( 1.0,  1.0,  1.0);
    glEnd();

  glColor3f(1.0,1.0,1.0);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, 1.0, -1.0);
    glVertex3f( -1.0, 1.0, 1.0);
    glVertex3f(-1.0,  -1.0, 1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
  glEnd();

  glColor3f(1.0,0.0,0.0);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();

  glColor3f(0.0,1.0,0.0);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, 1.0, 1.0);
    glVertex3f( -1.0, 1.0, -1.0);
    glVertex3f(1.0,  1.0, -1.0);
    glVertex3f( 1.0,  1.0, 1.0);
  glEnd();

  glColor3f(0.0,0.0,0.0);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f(1.0,  -1.0, -1.0);
    glVertex3f( 1.0,  -1.0, 1.0);
  glEnd();

}

void SolidCubeWidget::body() {
	this->cube();
  glTranslatef(0., 0., -3);
  glRotatef(90.0, 1.0, 0., 0.);
  // glScalef(0.5, 0.5, 1.0);
  this->cube();
}
	
// called every time the widget needs painting
void SolidCubeWidget::paintGL()
	{ // paintGL()
	// clear the widget
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
  // gluLookAt(1.,1.,1., 0.0,0.0,0.0, 0.0,1.0,0.0);
  gluLookAt(1.0, 1.0, 1.0, 
            0.0, 0.0, -1.0, 
            0.0, 1.0, 0.0);

  glRotatef(_time, 0., 0., 1);
  glRotatef(_time, 0., 1, 0.);
  this->body();
	
	// flush to screen
	glFlush();	

	} // paintGL()
