#include "SolidCubeWindow.h"

// constructor / destructor
SolidCubeWindow::SolidCubeWindow(QWidget *parent)
	: QWidget(parent)
	{ // constructor

	// create menu bar
	menuBar = new QMenuBar(this);

	// create the action
	actionQuit = new QAction("&Quit", this);

	// leave signals & slots to the controller

	// create the window layout
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);

	// create main widget
	cubeWidget = new SolidCubeWidget(this);
	windowLayout->addWidget(cubeWidget);

	// create slider
	nVerticesSlider = new QSlider(Qt::Horizontal);
	windowLayout->addWidget(nVerticesSlider);

	pTimer = new QTimer;
	pTimer->start(10);

	connect(pTimer, SIGNAL(timeout()), cubeWidget, SLOT(UpdateAngle()));
	} // constructor

SolidCubeWindow::~SolidCubeWindow()
	{ // destructor
	delete nVerticesSlider;
	delete cubeWidget;
	delete windowLayout;
	delete actionQuit;
	delete menuBar;
	} // destructor

// resets all the interface elements
void SolidCubeWindow::ResetInterface()
	{ // ResetInterface()
	nVerticesSlider->setMinimum(3);
	nVerticesSlider->setMaximum(30);

	//don't use the slider for now

	//	nVerticesSlider->setValue(thePolygon->nVertices);
	
	// now force refresh
	cubeWidget->update();
	update();
	} // ResetInterface()
