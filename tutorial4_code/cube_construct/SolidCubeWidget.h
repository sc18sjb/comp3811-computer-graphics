#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <QImage>
#include "Image.h"


class SolidCubeWidget: public QGLWidget
	{ // 

	Q_OBJECT

	public:
	SolidCubeWidget(QWidget *parent);

	public slots:
		void UpdateAngle();
		
	protected:
	// called when OpenGL context is set up
	void initializeGL();
	
	// called every time the widget is resized
	void resizeGL(int w, int h);

	void body();

	// called every time the widget needs painting
	void paintGL();

	private:

	void cube();
	void polygon(int, int, int, int);

	Image   _image;

        QImage* p_qimage;

	}; // class GLPolygonWidget
	
#endif
