#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <QDesktopWidget>
#include <QPoint>
#include <QObject>
#include <QTimer>
#include <iostream>

class MyWidget : public QWidget {
Q_OBJECT
public:
 MyWidget() {
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000);
 }

protected:
  void paintEvent( QPaintEvent * )
  {
    static const QPoint hourHand[3] = {
        QPoint(7, 8),
        QPoint(-7, 8),
        QPoint(0, -40)
    };

    static const QPoint minuteHand[3] = {
        QPoint(7, 8),
        QPoint(-7, 8),
        QPoint(0, -70)
    };
    
    QColor hourColor(127, 0, 127);
    QColor minuteColor(0, 127, 127, 191); // has alpha channel
    int side = qMin(width(), height());
    QTime time = QTime::currentTime();

    QPainter p( this );

    // turn on antialiasing
    p.setRenderHint(QPainter::Antialiasing);

    // move the origin to the center of the screen
    p.translate(width() / 2, height() / 2);
    p.scale(side / 200, side / 200);

    p.setPen( Qt::NoPen ); // We don't want any outline
    p.setBrush(hourColor);

    p.save();
    p.rotate(30.0 * ((time.hour() + time.minute() / 60.0)));
    p.drawConvexPolygon(hourHand, 3);
    p.restore();

    p.setPen(hourColor);
    for (int i=0; i<12; ++i) {
        p.drawLine(88, 0, 96, 0);
        p.rotate(30.0);
    }

    p.setPen(Qt::NoPen);
    p.setBrush(minuteColor);

    p.save();
    p.rotate(6.0 * (time.minute() + time.second() / 60.0));
    p.drawConvexPolygon(minuteHand, 3);
    p.restore();

    p.setPen(minuteColor);
    for (int i=0; i<60; ++i) {
        if ((i % 5) != 0)
            p.drawLine(92, 0, 96, 0);
        p.rotate(6.0);
    }

  }

};
 

