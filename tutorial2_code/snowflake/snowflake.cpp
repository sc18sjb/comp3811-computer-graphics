
#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QDesktopWidget>
#include <iostream>
#include <string>

class MyWidget : public QWidget {
public:

  // mouse tracking must be enabled
  void EnableMouse();
  void AcceptFocus();

protected:

  virtual void paintEvent(QPaintEvent*);
  virtual void mouseMoveEvent(QMouseEvent*);
  virtual void keyPressEvent(QKeyEvent *);

private:

};

void MyWidget::AcceptFocus()
{
  this->setFocusPolicy(Qt::StrongFocus);
}

void MyWidget::EnableMouse()
{
  // you must set mouse tracking, it's off by default
  this->setMouseTracking(true);
}

void MyWidget::mouseMoveEvent(QMouseEvent *_event)
{
  if (this->rect().contains(_event->pos())) {
    // Mouse over Widget
    QMessageBox::information(this,"test","You have made the boogie man show up tonight!");
  }
  else {
    // Mouse out of Widget
  }
}

void MyWidget::keyPressEvent(QKeyEvent *_event)
{
  QMessageBox::information(this,"test", QKeySequence(_event->key()).toString());
}

void MyWidget::paintEvent( QPaintEvent * )
{
  QPainter p( this );
  p.setPen( Qt::darkGray );
  p.drawRect( 1,2, 5,4 );
  p.setPen( Qt::lightGray );
  p.drawLine( 9,2, 7,7 );
}


// I don't like the default way the main window pops up, I want it bigger
class MyMainWindow : public QMainWindow {

public: 
  
  MyMainWindow();

};

MyMainWindow::MyMainWindow(){
  // my window asks how big the desktop is; this is a widget for QT
  resize(QDesktopWidget().availableGeometry(this).size() * 0.7);
  // I've set my window to 70 % of the desktop
}

int main(int argc, char **argv) {
  QApplication app(argc, argv);
  MyMainWindow window;

  MyWidget w;
  w.EnableMouse(); // don't forget this
  w.AcceptFocus();

  // We make MyWidget (QWidget) w the central widget of the window,
  // so that the main window can response to mouse events
  window.setCentralWidget(&w);
  window.show();

  // Start execution of the program
  return app.exec();
}
