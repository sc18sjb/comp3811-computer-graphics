#ifndef _MYCLASS
#define _MYCLASS
#include <QObject>


class MyClass : public QObject {
Q_OBJECT
public:
  MyClass();
public slots: // this is QT specific and not standard C++ (QT defines slots as a macro) 
  void handleButton();
private:
  std::string initializeWorkingDirectory() const;
  std::string _working_directory;
};

#endif // _MYCLASS
